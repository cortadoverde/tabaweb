<!DOCTYPE html>
<html ng-app="TabaApp">
  <head>
    <meta charset="utf-8" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="shortcut icon" sizes="196x196" href="assets/img/icon-196x196.png">



    <script src="/assets/js/jquery/dist/jquery.min.js"></script>
    <script src="/assets/js/lodash.min.js"></script>

    <?php if( isset( $_GET['sn'] ) ) : ?>
      <script src="/assets/bower_components/semantic-ui/dist/semantic.min.js"></script>
    <?php else : ?>
      <script src="/assets/semantic.min.js"></script>
    <?php endif ; ?>

    <script src="/assets/js/mousetrap.min.js"></script>
    <script src="/assets/js/angular.min.js"></script>
    <script src="/assets/js/angular-route.min.js"></script>
    <script src="/assets/bower_components/ngDialog/js/ngDialog.min.js"></script>
    <script src="/assets/bower_components/slideout.js/dist/slideout.min.js"></script>

    <script type="text/javascript">
      var site_base = "<?php echo $_SERVER['SERVER_NAME']?>";
    </script>
    <script src="dist/js/taba.js"></script>

    <?php if( isset( $_GET['sn'] ) ) : ?>
      <link rel="stylesheet" href="/assets/bower_components/semantic-ui/dist/semantic.css">
    <?php else : ?>
      <link rel="stylesheet" href="assets/semantic.css">
    <?php endif ; ?>

    <link rel="stylesheet" href="assets/css/theme.css">
    <link rel="stylesheet" href="/assets/bower_components/ngDialog/css/ngDialog.min.css">
    <link rel="stylesheet" href="/assets/bower_components/ngDialog/css/ngDialog-theme-default.min.css">
    <link rel="stylesheet" href="/assets/bower_components/ngDialog/css/ngDialog-custom-width.css">
    <link rel="stylesheet" href="/assets/bower_components/slideout.js/index.css">

    <script>
      $(function(){
        var menu_is_open = false;
        var slideout = new Slideout({
          'panel': document.getElementById('content_panel'),
          'menu': document.getElementById('mobile_menu'),
          'padding': 256,
          'tolerance': 70,
          'className' : 'test'
        });

        slideout.on('open',function(){
          $(this.panel).addClass('open_menu')
        })

        slideout.on('close',function(){
          $(this.panel).removeClass('open_menu')
        })

        $('.toggle-button').on('click',function(e) {
          e.preventDefault();
          e.stopPropagation();
          slideout.toggle();

        })

        $('#content_panel').on('click', function(e){

          if(slideout.isOpen()) {
            slideout.close();
          }
        })

        $(document).on('changeRoute', function(){
          slideout.close();
        })

      })

    </script>

    <title>{{ empresa.EMPRESA }}</title>

  </head>
  <body resizable ng-controller="tabaCtrl">

      <div class="ui active inverted dimmer"  ng-if="taba.state.loading">
        <div class="ui text loader">Cargando</div>
      </div>

      <nav id="mobile_menu">
        <div taba-menu ng-show="auth.logged"></div>
      </nav>

      <main id="content_panel">
        <div ng-include="'partials/header.html'" onload="checkUser()" class="fixnav"></div>

        <header class="show_mobile">
          <button class="button toggle-button">
            <i class="sidebar icon"></i> Abrir menú
          </button>
        </header>


        <div taba-menu ng-show="auth.logged" class="show_main_menu"></div>

        <div ng-view></div>

      </main>
  </body>
</html>
