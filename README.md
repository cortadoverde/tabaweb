# Instalacion

#### Clonar el repositorio

```
git clone git@bitbucket.org:tabasoftware/tabaweb.git

```

#### Instalar las dependencias para compilar los scripts

```
npm install

```

#### Tareas con gulp

- gulp => compila los scripts y los scss
- gulp watch => queda en espera de cualquier modificacion y compila


-------------------------------------------------------------------------------

# Taba Web
Este desarrollo consiste en una aplicacion AngularJs para la gestion de:

- Notas de pedidos
- disponibilidad de productos
- Descarga de listas de precios
- Resumen de cuenta corriente
- Saldo de Cuenta corriente
- Gestion de codigos de clientes


# A contemplar

- Lograr una estructura mas idonea para la modularización de las secciones
- Implementar vistas anidadas con ui.router
- Crear un manejador de eventos globales y temporales.
