
var app = angular.module('TabaApp', ['ngRoute', 'appControllers', 'appServices', 'appDirectives', 'taba.shortcuts', 'ngDialog']);

var appServices    = angular.module('appServices', []);
var appControllers = angular.module('appControllers', ['ngDialog','ngFileUpload']);
var appDirectives  = angular.module('appDirectives', []);

var options = {};
options.api = {};
options.api.base_url = "/api";

app.config(['$locationProvider', '$routeProvider',
  function($location, $routeProvider) {
    $routeProvider.

        when('/', {
            templateUrl: 'partials/ntavta.html',
            controller: 'PedidosCtrl',
            access: { requiredAuthentication: true }
        }).

        when('/login', {
            templateUrl: 'partials/user.signin.html',
            controller: 'AdminUserCtrl',
            show : { login: false }
        }).

        when('/logout', {
            templateUrl: 'partials/user.logout.html',
            controller: 'LogoutCtrl',
            access: { requiredAuthentication: true }
        }).
        when('/cuenta-corriente',{
            templateUrl: 'partials/cuenta-corriente.html',
            controller: 'CuentaCtrl',
            access : { requiredAuthentication : true }
        }).

        when('/ver-pedidos',{
            templateUrl: 'partials/pedidos_realizados.html',
            controller: 'VerPedidosCtrl',
            access : { requiredAuthentication : true }
        }).

        when('/disponibilidad',{
            templateUrl: 'partials/disponibilidad.html',
            controller: 'DisponibilidadCtrl',
            access : { requiredAuthentication : true }
        }).

        when('/archivos',{
            templateUrl: 'partials/descargas.html',
            controller: 'ArchivosCtrl',
            //access : { requiredAuthentication : true }
        }).

        when('/personalizar-codigos',{
            templateUrl: 'partials/personalizar-codigos.html',
            controller: 'CodigoClienteCtrl',
            access : { requiredAuthentication : true }
        }).

        otherwise({
            redirectTo: '/'
        });
}]);


app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
    $httpProvider.interceptors.push('StatusInterceptor');
});

app.run(function($rootScope, $location, $window, AuthenticationService, $http, UserService) {
    $rootScope.taba = {
      error : false,
      state : {
        loading: false,
        error: false
      },
      api   : options.api.base_url.replace('http://', '')
    }
    $rootScope.mode = {
        display: false,
        search: true
    }

    $rootScope.changeMode = function( type ) {
        if( type == 'display' ) {
            $rootScope.mode.display = true;
            $rootScope.mode.search = false;
        } else {
            $rootScope.mode.display = false;
            $rootScope.mode.search = true;
        }
    }

    $rootScope.loadInt = 0;

    $rootScope.AppReady = false;

    function logOut() {
        $rootScope.changeMode('search');
        UserService.logOut().success(function(data) {
            AuthenticationService.isAuthenticated = false;
            $rootScope.auth = {};
            $rootScope.auth.logged = false;
            delete $window.localStorage.token;
            $rootScope.$broadcast('auth.change')
        }).error(function(status, data) {
            delete $window.localStorage.token;
            $rootScope.$broadcast('auth.change')
        });
    }

    $rootScope.logOut = logOut;

    $rootScope.empresa = {};

    $http.get(options.api.base_url + '/about').success(function(data){
      $rootScope.empresa = data;
      $rootScope.$broadcast('empesa.ready');
    })

    if( $window.localStorage.token ) {
        UserService.me(function(data){
            AuthenticationService.isAuthenticated = true;
            $rootScope.$broadcast('auth.change')
        }, function(){
            $rootScope.$broadcast('auth.change')
        })

    }

    $rootScope.auth = {
        logged : false
    };

    console.log($rootScope.auth)

    $rootScope.$on('$routeChangeSuccess', function( event, currentRoute ) {
      if( currentRoute.$$route && currentRoute.$$route.controller ) {
        $rootScope.$broadcast('route.' + currentRoute.$$route.controller );
        console.log(currentRoute.$$route.controller );
      }
    })

    $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {

        if( nextRoute.$$route && nextRoute.$$route.templateUrl && nextRoute.$$route.templateUrl == 'partials/user.logout.html' ){
            logOut();
            $location.path("/login");
        }


        //redirect only if both isAuthenticated is false and no token is set
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
            && !AuthenticationService.isAuthenticated && !$window.localStorage.token) {
            $location.path("/login");
        }
        // redireccionar a la home si no ya esta logueado
        if( nextRoute != null && nextRoute.show != null && !nextRoute.show.login && !AuthenticationService.isAuthenticated && $window.localStorage.token ) {
          $location.path("/");
        }

    });


    // Eventos para indicar el estado de carga
    $rootScope.$on('http.request', function(){
      $rootScope.taba.state.loading = true;
    });

    $rootScope.$on('http.requestError', function(){
      $rootScope.taba.state.loading = false;
    })

    $rootScope.$on('http.response', function(){
      $rootScope.taba.state.loading = false;
    })

    $rootScope.$on('http.responseError', function(){
      $rootScope.taba.state.loading = false;
    })



});


app.filter('procesoEstado', function() {
    return function( estado ) {
        if( estado == 2 ) {
            return 'En proceso';
        }
        return 'completado';
    }
})

app.filter('procesoEstadoColor', function() {
    return function( estado ) {
        if( estado == 2 ) {
            return 'orange';
        }
        return 'green';
    }
})

app.filter('debe', function(){
    return function( value ) {
        if( value >= 0 ) {
            return value;
        }
        return null;
    }
})

app.filter('haber', function(){
    return function( value ) {
        if( value < 0 ) {
            return value * -1 ;
        }
        return null;
    }
});

app.filter('fileSize', function(){
  return function( bytes ) {
    var thresh = 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = ['kB','MB','GB','TB','PB','EB','ZB','YB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);

    return bytes.toFixed(1)+' '+units[u];
  }
})
app.filter('fileTypeIcon', function(){
  return function ( fileName ) {
    var ext = fileName.split('.').pop();
    var icons = {
      'pdf' : 'file pdf outline',
      'xls' : 'file excel outline',
      'xlsx' : 'file excel outline',
      'doc' : 'file word outline',
      'docx' : 'file word outline',
      'txt' : 'file text outline',
      'zip' : 'file archive outline',
      'rar' : 'file archive outline',
      'tar' : 'file archive outline',
      'gz' : 'file archive outline',
      '7z' : 'file archive outline',
      'jpg' : 'file image outline',
      'png' : 'file image outline',
      'gif' : 'file image outline',
      'jpeg' : 'file image outline',
      '_def' : 'file'
    };

    if( ! icons[ext] ) {
      return icons['_def'];
    }
    return icons[ext];

  }
})

appControllers.controller('AdminUserCtrl', ['$scope', '$location', '$window', 'UserService', 'AuthenticationService', '$rootScope',
    function AdminUserCtrl($scope, $location, $window, UserService, AuthenticationService, $rootScope) {

        //Admin User Controller (signIn, logOut)
        $scope.signIn = function signIn(username, password) {
            if (username != null && password != null) {

                var tipo = $scope.user.es_vendedor ? 'vendedor' : 'cliente';

                UserService.signIn(username, password, tipo).success(function(data) {
                    AuthenticationService.isAuthenticated = true;
                    $rootScope.auth.logged = true;
                    UserService.curretUser = data;
                    $window.localStorage.token = data.token;
                    $rootScope.$broadcast('auth.change');
                    //$window.open('/','myInstance','fullscreen=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no')
                }).error(function(data, status) {
                    if( typeof data.level !== 'undefined' ) {
                      $rootScope.taba.error = {
                       level : data.level,
                       content: data.error
                      };
                      setTimeout(function(){
                          $rootScope.taba.error = null;
                          $rootScope.$apply();
                      }, 3000);
                    }
                    $rootScope.$broadcast('auth.change')
                });

            }
        }


    }
]);

appControllers.controller('LogoutCtrl',function ($scope){

})

appDirectives.directive('sidebarBtn', function ($rootScope){
    return {
        restrict: 'AE',
        template: '<i class="icon list layout"></i> Menu',
        link : function( scope, elm, attr ) {
            var transition = "slide out";

            scope.$on('isMobile',function(){
                $('#m_menu').removeClass( transition );
                $('#m_menu').addClass("top");;
                $('#m_menu').addClass("labeled icon");
                $('#m_menu').sidebar('setting', {
                  dimPage          : false,
                  transition       : 'push',
                  mobileTransition : 'push'
                })
            })

            scope.$on('noMobile',function(){
               $('#m_menu').removeClass("top push");
               $('#m_menu').removeClass("labeled icon");

               $('#m_menu').sidebar('setting', {
                  dimPage          : false,
                  transition       : transition
                })
            })

            angular.element(elm).bind('click',function(e){
                $('#m_menu').sidebar('toggle');
            })
        }
    }

})

appDirectives.directive('focusMe', function($timeout) {
  return {
    scope: { trigger: '=focusMe' },
    link: function(scope, element) {
      scope.$watch('trigger', function(value) {
        if(value === true) {
            element[0].focus();
            scope.trigger = false;
        }
      });
    }
  };
});

appDirectives.directive('autofocusWhen', function ($timeout) {
    return {
        link: function(scope, element, attrs) {
            scope.$watch(attrs.autofocusWhen, function(newValue){
                if ( newValue ) {
                    $timeout(function(){
                        element.focus().select();
                    });
                }
            });
        }
     };
});

appDirectives.directive('tabaMenu', function( $rootScope, UserService, $compile ){

    return {
        restrict: 'A', // Atributo o Elemento ( <elm taba-menu> || <taba-menu> )
        template:     ' '
                    + ' '
                    + '  <div class="ui secondary pointing menu" id="menu"> '

                    + '      <span ng-repeat="item in taba.menu"> '
                    + '      <a href="#{{item.url}}" class="item {{item.align}}"> '

                    + '             <i class="{{item.icon}} icon"></i>{{item.title}} '

                    + '      </a> </span>'

                    + '  </div> '
                    + ' '
                    // + '<nav class="ui very wide sidebar menu vertical left slide out" id="m_menu">'
                    // + '      <span ng-repeat="item in taba.menu"> '
                    // + '      <a href="#{{item.url}}" class="item {{item.align}}" {{item.action}}> '

                    // + '             <i class="{{item.icon}} icon"></i>{{item.title}} - {{item.action}}'

                    // + '      </a> </span>'
                    // + '</nav> '
                //    + ' <div id="m_btn" sidebar-btn> '
                //    + ' </div>'
                    ,

        link: function (scope, elm, attrs ) {

        }
    }
})

appDirectives.directive('tabaMenuMobile', function() {
  return {
    restrict : 'A',
    templateUrl : 'partials/components/mobile_menu.html',
    link: function (scope, elm, attrs ) {

    }
  }
})


appDirectives.directive('resizable', function ($window, $rootScope){
    return function ($scope) {
        return angular.element($window).bind('resize', function() {
          var width = $window.innerWidth;
          if( width < 500 ) {
            $rootScope.$broadcast('isMobile');
          } else {
            $rootScope.$broadcast('notMobile');
          }
          return;
        }).trigger('resize');
    };
})

appDirectives.directive('showiniframe', function( $window, $rootScope ) {

  function link(scope, element, attrs) {
    var insideIframe = $window.top !== $window.self;

    if( insideIframe ) {
      element.css('display', 'block')
    } else {
      element.css('display', 'none')
    }
  }


  return {
    link : link
  }
});

appDirectives.directive('hideiniframe', function( $window, $rootScope ) {

  function link(scope, element, attrs) {
    var insideIframe = $window.top !== $window.self;

    if( ! insideIframe ) {
      element.css('display', 'block')
    } else {
      element.css('display', 'none')
    }
  }


  return {
    link : link
  }
})

appDirectives.directive('typeahead', function($timeout,$http, ApiService) {
  return {
    restrict: 'AEC',
    scope: {
      title: '@',
      retkey: '@',
      displaykey:'@',
      modeldisplay:'=',
      subtitle: '@',
      modelret: '='
    },

    link: function(scope, elem, attrs) {
        scope.current = 0;
        scope.selected = false;

      scope.da  = function(){
          scope.ajaxClass = 'loadImage';
          var txt = scope.modeldisplay
          ApiService.findCuenta(txt).success(function(data, status){
                scope.TypeAheadData = data;
                scope.ajaxClass = '';
          })

      }

      scope.handleSelection = function(key,val) {
        scope.modelret = key;
        scope.modeldisplay = val;
        scope.current = 0;
        scope.selected = true;
        scope.TypeAheadData = '';
      }

      scope.isCurrent = function(index) {
        return scope.current == index;
      }

      scope.setCurrent = function(index) {
        scope.current = index;
      }

    },
    template:
        '<div class="ui icon input">'
            +'<input type="text" ng-model="modeldisplay" ng-keyup="da(modeldisplay)"  ng-keyup="da(modeldisplay)"  ng-keydown="selected=false" ng-class="ajaxClass" /> '
        +'</div>'
        +'<div class="list-group table-condensed overlap" ng-hide="!modeldisplay.length || selected" style="width:100%">'
            +'<a class="list-group-item noTopBottomPad" ng-repeat="item in TypeAheadData|filter:model  track by $index" '+
                       'ng-click="handleSelection(item[retkey],item[displaykey])" style="cursor:pointer" '+
                       'ng-class="{active:isCurrent($index)}" '+
                       'ng-mouseenter="setCurrent($index)">'+
                         ' {{item[title]}}<br />'+
                         '<i>{{item[subtitle]}} </i>'+
                    '</a> '+
        '</div>'
  };
});

appDirectives.directive('onReadFile', function ( $parse ) {
  return {
    restrict : 'A',
    scope : false,
    link : function ( scope, element, attrs ) {
      var fn = $parse(attrs.onReadFile);

      element.on('change', function(onChangeEvent) {
          var reader = new FileReader();

          reader.onload = function ( onLoadEvent ) {
            if( onLoadEvent.target.result != '' ) {
              scope.$apply( function() {
                element.val('');
                fn( scope, { $fileContent: onLoadEvent.target.result } );
              });
            }
          };

          reader.readAsBinaryString((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
      })
    }
  }
})

appServices.factory('AuthenticationService', function () {
    var auth = {
        isLogged: false
    }
    return auth;
});

appServices.factory('TokenInterceptor', function ($q, $window, $location, AuthenticationService, $rootScope) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.localStorage.token) {
                config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
            }
            return config;
        },

        requestError: function(rejection) {
            return $q.reject(rejection);
        },

        /* Set Authentication.isAuthenticated to true if 200 received */
        response: function (response) {
            if (response != null && response.status == 200 && $window.localStorage.token && !AuthenticationService.isAuthenticated) {
                AuthenticationService.isAuthenticated = true;
                $rootScope.$broadcast('auth.change')
            }
            return response || $q.when(response);
        },

        /* Revoke client authentication if 401 is received */
        responseError: function(rejection) {
            if (rejection != null && rejection.status === 401 && ($window.localStorage.token || AuthenticationService.isAuthenticated)) {
                delete $window.localStorage.token;
                AuthenticationService.isAuthenticated = false;
                $rootScope.$broadcast('auth.change')
                $location.path("/login");
            }

            return $q.reject(rejection);
        },
    };
});

appServices.factory('UserService', function ($http) {
    return {

        currentUser: false,

        signIn: function(username, password, tipo) {
            return $http.post(options.api.base_url + '/user/login', {cuenta: username, clave: password, tipo: tipo});
        },

        logOut: function() {
            return $http.get(options.api.base_url + '/user/logout');
        },

        me: function( success, error ) {
            return $http.get(options.api.base_url + '/me?uuid=' + Math.floor(Math.random()*1000) +'-'+Date.now())
                        .success(success)
                        .error(error)
        }
    }
});

appServices.factory('ApiService', function ($http, $rootScope) {
    return {

        cuenta : null,

        findCuenta: function( data ) {
            return $http.get(options.api.base_url + '/cuenta/find?cuenta=' + encodeURIComponent( data ) );
        },

        setCuenta: function( cuenta ) {
            return $http.post(options.api.base_url + '/cuenta/set/' + cuenta);
        },

        ctacte : function( err, success) {
        },

        pedidosRealizados: function( ) {
          return $http.get(options.api.base_url + '/mis-pedidos');
        },

        pedidosPendientes : function() {
          return $http.get(options.api.base_url + '/pedidos/pendientes');
        },

        pedidoConfirmar: function() {
            return $http.post(options.api.base_url + '/pedidos/confirmar')
        },

        pedidosEliminarRegistro: function( registro ) {
          return $http.delete(options.api.base_url + '/pedidos/pendientes/' + registro );
        },

        pedidosCancelar: function( ) {
            return $http.delete(options.api.base_url + '/pedidos/pendientes');
        },

        buscarProductoPorCodigo : function( data ) {
            return $http.get(options.api.base_url + '/productos/buscar?codProducto=' + encodeURIComponent( data ))
        },

        buscarProductoPorCodigoCliente : function( data ) {
            return $http.get(options.api.base_url + '/productos/buscar?codCliente=' + encodeURIComponent( data ))
        },

        crearPedido : function( obj ) {
            return $http.post(options.api.base_url + '/pedidos/crear', { productos : obj } );
        },

        crearCodigo : function( obj ) {
            return $http.post(options.api.base_url + '/productos/crearCodigo', { codigos : obj } );
        },

        // Stock
        stock : function( type, str ) {
            return $http.get(options.api.base_url + '/stock?type=' + type + '&str=' + str)
        },

        // Cuenta
        cuenta: function( type ) {
            return $http.get(options.api.base_url + '/cuenta?type=' + type )
        },

        comprobante : function( tipoId, numeroId ) {
            return  $http.get(options.api.base_url + '/comprobante/' + tipoId + '/' + numeroId )
        },

        archivos : function( ) {
          return $http.get(options.api.base_url + '/descargas' );
        },

        removerArchivo : function( file_id ) {
          return $http.get(options.api.base_url + '/descargas/delete/' + file_id );
        },

        createGrupo : function ( group_data ) {
          return $http.post( options.api.base_url + '/crear-grupo', { group : group_data })
        }


    }
});

/**
 * Manejador de estados de la peticion actual,
 * genera un evento que se pueda capturar para mostrar notificaciones en la
 * aplicacion
 */
appServices.factory('StatusInterceptor', function( $q, $rootScope ) {
  return {

    request: function( config ) {
      $rootScope.$broadcast('http.request', arguments);
      return config;
    },

    requestError : function(rejection) {
      $rootScope.$broadcast('http.requestError', rejection);
      return $q.reject(rejection);
    },

    response: function (response) {
        $rootScope.$broadcast('http.response', response);
        return response || $q.when(response);
    },

    responseError: function(rejection) {
        $rootScope.$broadcast('http.responseError', rejection);
        return $q.reject(rejection);
    }

  }
})

appControllers.controller('ArchivosCtrl', function ( $rootScope, $scope, ApiService, ngDialog, Upload, $window ){
  $scope.groups = null;
  $scope._actives = {};

  ApiService.archivos()
      .success(function(items){
          $scope.groups = items;
          console.log(items);
      })
      .error(function( data, status){
      })


  $scope.download = function( fileData ) {
    $window.location.href = options.api.base_url + '/download/' + fileData._id;
    //$location.replace();
  }

  $scope.remove_file = function( file_id, group_id ) {
    var updateGroupId = _.findIndex( $scope.groups, function( group ) {
      return group._id == group_id;
    });

    ApiService.removerArchivo(file_id)
      .success(function( data ){
          $scope.groups[updateGroupId] = data;
      })
  }

  $scope.createGroup = function() {

    $scope.abm_grupo = {
      _action : "Crear nuevo "

    }

    $scope.dialog = ngDialog.open({
        template : 'partials/createGroup.html',
        className: 'ngdialog-theme-default',
        scope : $scope
    })
  }

  $scope.saveGroup = function() {
      ApiService.createGrupo($scope.abm_grupo)
        .success(function( response ) {
          $scope.groups.push(response);
        })
      $scope.dialog.close();
  }

  $scope.deleteGrupo = function( grupo )
  {
    $scope.groups = _.without($scope.groups,grupo)
  }

  $scope.active_upload = function( group_id ) {
    return typeof $scope._actives[group_id]  !== "undefined";
  }

  $scope.progress = function( group_id ) {
    return $scope._actives[group_id];
  }
  $scope.upload = function (files, grupo) {
      $scope._actives[grupo] = true;
      var updateGroupId = _.findIndex( $scope.groups, function( group ) {
        return group._id == grupo;
      });


      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          var UploadHandler = Upload.upload({
            url: options.api.base_url + '/descargas/upload',
            file: file,
            fields : {
              group_id : grupo
            }
          });

          UploadHandler.progress( function (evt) {
            $scope._actives[grupo] = parseInt(100.0 * evt.loaded / evt.total);
          })

          UploadHandler.success( function (data, status, headers, config) {
            $scope.groups[updateGroupId] = data;
            delete $scope._actives[grupo];

          })

          UploadHandler.error( function() {
          })

        }
      }
      /*
      if (files && files.length) {
          for (var i = 0; i < files.length; i++) {
              var file = files[i];
              Upload.upload({
                  url: options.api.base_url + '/descargas/upload',
                  file: file
              }).progress(function (evt) {
                  var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                  console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
              }).success(function (data, status, headers, config) {
                  $scope.showContent(data);
                  console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
              }).error(function (data, status, headers, config) {
                  console.log('error status: ' + status);
              })
          }
      }
      */
  };

});


appControllers.controller('CodigoClienteCtrl', function ( $rootScope, $scope, ApiService, ngDialog, Upload){
    $scope.code = {};

    $scope.buscarCodigoProducto = function() {
        ApiService.buscarProductoPorCodigo( $scope.search )
            .success(function( items ) {
                $scope.resultados = items;
            })
    }

    $scope.crearCodigo = function() {

        ApiService.crearCodigo( $scope.code )
            .success(function( ) {
                $scope.resultados = [];
            })
    }

    $scope.evalKey = function( $event ) {
        if( $event.keyCode && $event.keyCode == '13' ) {
            $scope.buscarCodigoProducto();
        }
    }

    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: options.api.base_url + '/upload',
                    fields: {'username': 'tabauploader'},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    $scope.showContent(data);
                }).error(function (data, status, headers, config) {
                    console.log('error status: ' + status);
                })
            }
        }
    };


    $scope.showContent = function(result){

        $scope.checkAllItemes = true;

        $scope.columna = {
            a : 'codigoProducto',
            b : 'codigoCliente',
            c : 'nombre'
        };

        $scope.cellOrder = {};

        $scope.cells = result;

        $scope.dialog = ngDialog.open({
            template : 'partials/import_excel_codigo.html',
            className: 'ngdialog-theme-default',
            scope : $scope
        })

    };

    $scope.importarExcel = function() {
        var o = {};
        var error = [];

        console.log( $scope.columna );
        var colCodProducto = _.findKey( $scope.columna, function( str ) { return str == 'codigoProducto' } ).toUpperCase();
        var colCodCliente  = _.findKey( $scope.columna, function( str ) { return str == 'codigoCliente' } ).toUpperCase();
        var colDetalle     = _.findKey( $scope.columna, function( str ) { return str == 'nombre' } ).toUpperCase();

        _.forEach($scope.cellOrder, function(obj, index) {
            if( obj.import ) {
                var codProducto = obj[colCodProducto].toString().toUpperCase();
                var codCliente  = obj[colCodCliente];
                var detalle     = obj[colDetalle];

                if( ! o.hasOwnProperty( codProducto ) ) {
                    o[codProducto] = {
                        codigo: codCliente,
                        nombre : detalle
                    }
                }
            }
        });

        if( ! _.isEmpty( o ) &&  error.length == 0 ) {
            ApiService.crearCodigo( o )
                .success(function( ) {
                    $scope.dialog.close();
                    $scope.buscarCodigoProducto();
                })
        } else {
            alert( error.join('\n') );
        }
    }

    $scope.checkAll = function() {
        console.log( $scope.all );
        if( $scope.all ) {
            $scope.all = false;
        } else {
            $scope.all = true;
        }

        console.log($scope.checkAllItemes);
        console.log($scope);

        _.forEach($scope.cellOrder,function(n, idx){
            $scope.cellOrder[idx].import = $scope.all;
        });
    }

});


appControllers.controller('CuentaCtrl', function($scope, $rootScope, ApiService, ngDialog){
    $scope.pendings = [];
    $scope.resumen = [];
    $scope.state = 'pending';
    $scope.switchTo = 'resumen';

    $scope._load = function() {
        ApiService.cuenta($scope.state)
            .success(function( data ){
                if( $scope.state == 'pending' ) {
                    $scope.pendings = data;
                } else {
                    $scope.resumenSaldo = data.SaldoInicial;
                    $scope.resumen = data.Items;
                }
            })
    }

    $scope.$watch('state', function(n, o){
        if( n == 'pending' ) {
            $scope.resumen = [];
            $scope.resumenSaldo = 0;
            $scope.switchTo = 'Resumen de cuenta corriente';
        } else {
            $scope.pendings = [];
            $scope.switchTo = 'Pendientes por cobrar';
        }
        $scope._load();
    })

    $rootScope.$on('app.ready', function(){
        $scope._load();
    })

    $scope.changeState = function() {
        $scope.state = $scope.state == 'pending' ? 'resumen' : 'pending';
    }

    $scope.calcSaldo = function( index ) {
       var totales =  _.pluck( $scope.pendings, 'Saldo' ) ;
       return _.sum(totales.slice(0, index + 1 ));
    }

    $scope.calcSaldoResumen = function( index ) {
       var totales =  _.pluck( $scope.resumen, 'ImporteCbte' ) ;
       return  $scope.resumenSaldo + _.sum(totales.slice(0, index + 1 ));
    }

    $scope.ver_comprobante = function(event, tipoId, numeroId ) {
        event.preventDefault();

        ApiService.comprobante(tipoId, numeroId)
            .success(function( comprobante ){
                $scope.factura = comprobante;
                ngDialog.open({
                    template : 'partials/comprobante.html',
                    className: 'ngdialog-theme-default',
                    scope : $scope
                })
            })
    }




});


appControllers.controller('DisponibilidadCtrl', function ( $rootScope, $scope, ApiService){
    $scope.items = [];
    $scope.search = '';
    $scope.showPrice =  false;
    $scope.showCantidad = false;

    // Notificaciones globales
    // formato
    // {
    //   * type : [danger|warning|info],
    //   * body : String,
    //   icon   : [icono],
    //   header : String
    // }
    $scope.notificaciones = [];

    // Control de errores de salida, para mantener una integridad
    var errorCodes = {
      1 : 'No se encontraro registros'
    }

    var chk = function() {
        // $scope.showPrice = $rootScope.auth._type == 'vendedor' ? true : $rootScope.empresa.verPrecio;
        $scope.showCantidad = $rootScope.auth._type == 'vendedor' ? true : $rootScope.empresa.verCantidad;
    }

    function processSearchResult( items ) {
      if( items.length == 0 ) {
        $scope.notificaciones.push({
          _errorCode : 1,
          type : 'warning',
          icon : 'warning',
          header: errorCodes[1],
          body: 'Verifique el código de producto y vuelva a intentarlo, busqueda actual: ' + $scope.search
        })
      } else {
        _.remove($scope.notificaciones, { _errorCode : 1 } )
      }
      $scope.items = items;
    }

    chk();

    $scope.$on('$viewContentLoaded', function(){
      chk();
      console.log('trigger $viewContentLoaded')
    })
    $rootScope.$on('route.DisponibilidadCtrl',function(){
      chk();
      console.log('trigger route.DisponibilidadCtrl')
    })

    $rootScope.$on('app.ready', function(){
        chk();
        console.log('trigger app ready')
    })


    $scope.buscarCodigoProducto = function() {
        ApiService.stock('codProducto', $scope.search )
            .success(processSearchResult)
    }

    $scope.buscarCodigoCliente = function() {
        ApiService.stock('codCliente', $scope.search )
            .success(processSearchResult)
    }

    $scope.getDisplay = function( item ) {
        if( item.stockactual1 > item.stockminimo ) {
            return '<b>disponible</b>';
        } else {
            if( item.stockactual1  > 0 ) {
                return '<b>consultar</b>';
            } else {
                return '<b>No disponible</b>';
            }
        }
    }

    $scope.evalKey = function( event ) {
        if( event.keyCode && event.keyCode == '13' ) {
            $scope.buscarCodigoProducto();
        }
    }
});

appControllers.controller('findUserCtrl', function ($scope, $rootScope, ApiService, $timeout, $location) {

    $scope.list = [];
    $scope.selected = false;
    $scope.search = '';
    $scope.focusInput = true;

    $scope.reset = function() {
        $scope.list = [];
        $scope.selected = false;
        $scope.search = '';
        $scope.focusInput = true;
    }

    $rootScope.$on('auth.change', function(){
        $scope.reset();
    })

    $scope.find = function ( str ) {
        ApiService.findCuenta(str).success(function (data, status) {
            $scope.list = data;
        })
    }

    $scope.selectElm = function( item ) {
        $scope.selected = item;
        //$scope.search = item.Cuenta ;
        $rootScope.mode.search = false;
        $rootScope.mode.display = true;
        $scope.focusInput = false;
        ApiService.setCuenta(item.Cuenta).success(function (data, status) {
            $rootScope.auth._current = data;
            // console
            console.log($location.path());
            $rootScope.$broadcast('app.ready');
        })
    }

    $scope.keydown = function( event ) {
        $scope.selected=false;
        if( event.keyCode == 27 ) {
            $scope.list = [];
            $rootScope.changeMode('display');
        }
        if( event.keyCode == 9 ) {
            $('.searchResults').focus();
            event.preventDefault();
        }
    }

    $scope.selectByEnter = function( event, item) {
        if( event.keyCode == 13 ) {
            $scope.selectElm(item)
        }
    }

    $scope.editMode = function() {
        if( $rootScope.auth._type == 'vendedor' ) {
            $rootScope.changeMode('search');
            $scope.focusInput = true;
        }
        //test
    }

    $scope.$watch('search', function(str){
        if( str.length > 2 )
            $scope.find(str);
        else
            $scope.list = []
    })


});

/**
 * Controlador de Pedidos
 * Gestiona las solicitudes de los clientes, interactua con el servicio: @ApiService
 * que envia request a la url de la API del Taba
 */
appControllers.controller('PedidosCtrl', function ($scope, ApiService, $rootScope, ngDialog, Upload) {
    // Pendientes a procesar, se cargara con los datos obtenidos de la api
    // esta variable esta constantemente actualizandose
    $scope.pendings = [];

    // resultados de busqueda
    $scope.resultados = [];

    // Pedido actual
    $scope.order = {};

    // Variable de control de muestra precio,
    // a futuro debera validarse desde opciones de un panel de administracion
    $scope.showPrice =  false;

    // Variable de control para saber si en la etiqueta de color indica la cantidad
    // disponible de un producto, esta se activa actualmente si el usuario es vendedor
    $scope.showCantidad = false;

    // Variable de control para el importador para saber si los checkbox de cada fila
    // se activan o no
    $scope.all = true;

    // Notificaciones globales
    // formato
    // {
    //   * type : [danger|warning|info],
    //   * body : String,
    //   icon   : [icono],
    //   header : String
    // }
    $scope.notificaciones = [];

    // Errores De importacion
    // formato
    // {
    //   codProducto
    //   message
    // }
    $scope.errores_importacion = [];

    // Control de errores de salida, para mantener una integridad
    var errorCodes = {
      1 : 'No se encontraro registros',
      2 : 'La "cantidad" debe ser un número'
    }

    // Metodo para procesar la respuesta del callback de la API de busqueda de productos, ya sea por codigo
    // de producto o por codigo de cliente
    function processSearchResult( items ) {
      $scope.notificaciones = [];
      if( items.length == 0 ) {
        $scope.notificaciones.push({
          _errorCode : 1,
          type : 'warning',
          icon : 'warning',
          header: errorCodes[1],
          body: 'Verifique el código de producto y vuelva a intentarlo, busqueda actual: ' + $scope.search
        })
      } else {
        _.remove($scope.notificaciones, { _errorCode : 1 } )
      }
      $scope.resultados = items;
    }

    // funcion de comprobacion para las variables de control
    var chk = function() {
        // $scope.showPrice = $rootScope.auth._type == 'vendedor' ? true : $rootScope.empresa.verPrecio;
        $scope.showCantidad = $rootScope.auth._type == 'vendedor' ? true : $rootScope.empresa.verCantidad;
    }

    // Evento principal
    // se dispara cuando la aplicacion se ha terminado de cargar
    $rootScope.$on('app.ready', function(){
        chk();
        $scope.getPendings();
    })

    // Inicialmente busca pedidos pendientes
    ApiService.pedidosPendientes()
      .success(function(items){
          chk();
          $scope.pendings = items;
      })
      .error(function( data, status){
      })

    // Elimina la notificaion al pulsar en la cruz
    $scope.removeNotification = function( idx ) {
      $scope.notificaciones.splice(idx, 1);
    }

    // Elimina el mensaje de error
    $scope.removeError = function( idx ) {
      $scope.errores_importacion.splice(idx, 1);
    }

    // Hace un request a la API y obtiene los
    // pedidos que no se enviaron a procesar todavia
    $scope.getPendings = function() {

        $scope.search = '';

        ApiService.pedidosPendientes()
        .success(function(items){
            $scope.pendings = items;
        })
        .error(function( data, status){
            $scope.pendings = [];
        })
    }

    // Confirma el pedido pendiente y pasa a pedidos realizados
    $scope.confirm = function() {
        ApiService.pedidoConfirmar()
            .success(function(data){
                $scope.getPendings();
            })
    }

    // Elimina todos los pedidos pendientes
    $scope.deleteAll = function(e) {
        e.preventDefault()
        if( confirm(' Seguro que desea eliminar este registro ?') ) {
            ApiService.pedidosCancelar()
                .success(function(data){
                    $scope.getPendings();
                })
        }
    }

    // Elimina un pendiente de la lista actual
    $scope.deleteItem = function( registro ) {
        if( confirm(' Seguro que desea eliminar este registro ?') ) {
            ApiService.pedidosEliminarRegistro( registro )
                .success(function(){
                    $scope.getPendings();
                })
        }
    }

    // Envia una peticion a la API para buscar productos por codigo de producto
    $scope.buscarCodigoProducto = function() {
        ApiService.buscarProductoPorCodigo( $scope.search )
            .success(processSearchResult)
    }

    // Similar a buscarCodigoProducto pero con el codigo de cliente
    $scope.buscarCodigoCliente = function() {
        ApiService.buscarProductoPorCodigoCliente( $scope.search )
            .success(processSearchResult)
    }

    // Crea un pedido pendiente con los datos de seleccion
    // de la busqueda de productos
    $scope.crearPedido = function() {
       ApiService.crearPedido( $scope.order )
          .success(function( data ) {
              $scope.resultados = [];
              $scope.order = {};
              $scope.getPendings();
          })
    }

    // Evalua el evento keyup para el input del buscador
    $scope.evalKey = function( event ) {
        if( event.keyCode && event.keyCode == '13' ) {
            $scope.buscarCodigoProducto();
        }
    }

    $scope.evalKeyItem = function( $event, validation ) {
        console.log(validation);
        if( validation && validation == 'number' ) {
          if(!(($event.keyCode > 95 && $event.keyCode < 106)
            || ($event.keyCode > 47 && $event.keyCode < 58)
            || $event.keyCode == 8)) {
              $event.preventDefault();
              return false;
          }
        }

        if( $event.keyCode && $event.keyCode == '13' ) {
            $scope.crearPedido();
        }
    }

    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: options.api.base_url + '/upload',
                    fields: {'username': 'tabauploader'},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    $scope.showContent(data);
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                }).error(function (data, status, headers, config) {
                    console.log('error status: ' + status);
                })
            }
        }
    };

    $scope.importarExcel = function() {
        var o     = {};
        var error = [];
        var n     = 0;
        $scope.errores_importacion = [];

        var colCodProducto = _.findKey( $scope.columna, function( str ) { return str == 'codProducto' } ).toUpperCase();
        var colCantidad    = _.findKey( $scope.columna, function( str ) { return str == 'cantidad' } ).toUpperCase();
        var colDetalle     = _.findKey( $scope.columna, function( str ) { return str == 'detalle' } ).toUpperCase();

        _.forEach($scope.cellOrder, function(obj, index) {
            if( obj.import ) {
                n++;

                if( typeof obj[colCodProducto] == 'undefined' ) return;

                var codProducto = obj[colCodProducto].toString().toUpperCase();
                var cantidad = obj[colCantidad] ;
                var detalle  = obj[colDetalle];

                if( _.trim(codProducto).length == 0 ) return;

                if( isNaN( cantidad ) ) {
                    cantidad = 0
                }
                cantidad = parseInt( cantidad );

                if( ! o.hasOwnProperty( codProducto ) ) {
                    o[codProducto] = {
                        cantidad: cantidad,
                        detalle : detalle
                    }
                } else {
                    o[codProducto].cantidad = parseInt(o[codProducto].cantidad) + parseInt(cantidad);
                   // o[codProducto].detalle = o[codProducto].detalle + ' / ' + detalle;
                }

                if( o[codProducto].cantidad < 1 )
                  delete o[codProducto];

            }

        })

        if( error.length > 0 ) {
          $scope.notificaciones = _.extend($scope.notificaciones, error);
        }

        if( error.length == n ) {
          $scope.dialog.close();
          return false;
        }



        ApiService.crearPedido( o )
            .success(function( data ) {
                $scope.getPendings();
                console.log(data);
                if( typeof data.error !== "undefined" && data.error.length > 0 )
                  $scope.errores_importacion = data.error;
                $scope.dialog.close()
            })
    }

    $scope.checkAll = function() {
        console.log( $scope.all );
        if( $scope.all ) {
            $scope.all = false;
        } else {
            $scope.all = true;
        }


        _.forEach($scope.cellOrder,function(n, idx){
            $scope.cellOrder[idx].import = $scope.all;
        });
    }

    $scope.clearNotificaciones = function() {
      $scope.notificaciones = [];
      $scope.errores_importacion = [];
    }


    $scope.showContent = function(result){

        $scope.checkAllItemes = true;


        $scope.columna = {
            a : 'codProducto',
            b : 'cantidad',
            c : 'detalle'
        };

        $scope.cellOrder = {};

        $scope.cells = result;

        $scope.dialog = ngDialog.open({
            template : 'partials/import_excel.html',
            className: 'ngdialog-theme-default',
            scope : $scope
        })

    };
});


appControllers.controller('tabaCtrl', function ($scope, $rootScope, AuthenticationService, UserService, $location, shortcuts){

    $scope.taba = $scope.taba || {};
    $scope.taba.menu = [];

    var menu = [
       // scope.taba.menu = [
        {
            url : 'realizar-pedidos',
            title: 'Nuevo Pedido',
            perms: 2,
            icon: 'file text outline'
        },
        {
            url: 'ver-pedidos',
            title: 'Pedidos realizados',
            perms : 2,
            icon: 'archive'
        },
        {
            url : 'disponibilidad',
            title : 'Disponibilidad',
            perms: 1,
            icon: 'cubes'
        },
        {
            url: 'archivos',
            title: 'Descargas',
            perms: 0,
            icon: 'cloud download'
        },
        {
            url : 'cuenta-corriente',
            title: 'Cuenta Corriente',
            perms: 1,
            icon: 'money'
        },
        {
            url : 'personalizar-codigos',
            title: 'Código Cliente',
            perms: 2,
            icon: 'barcode'
        },
        {
            url : 'logout',
            title: 'Salir',
            perms: 0,
            align: 'right',
            icon: 'sign out',
            action: 'ng-click="userLogout()"'
        }
    ];

    $rootScope.$on('apprun', function(){
    })

    $rootScope.$on('app.ready', function(){
        $rootScope.$broadcast('taba.menu');
    })

    $rootScope.$on('taba.menu', function() {
        var redirect;
        $scope.taba.menu = [];
        if( $rootScope.auth.logged ) {
            angular.forEach(menu, function(item){
                if( $rootScope.auth._perms >= item.perms  ) {
                    if( ( ! $rootScope.auth._current  && item.perms == 0 ) || $rootScope.auth._current ) {
                        if( typeof redirect == 'undefined' ) {
                            redirect = item.url;
                        }
                        $scope.taba.menu.push(item);
                    }
                }
            })

            if( $rootScope.auth && $rootScope.auth._current ) {
                $rootScope.changeMode('display');
            }

        }
    })

    $rootScope.$on('auth.change', function(){
        if( AuthenticationService.isAuthenticated ) {
            UserService.me(function (data){
                $rootScope.auth = data;
                $rootScope.auth.logged = true;
                $rootScope.auth.is_admin = function() {
                  if( typeof this.Vendedor !== 'undefined' )
                    return true;

                  return false;
                }
                $rootScope.$broadcast('app.ready');

            }, function (err) {
                $rootScope.$broadcast('app.ready');
            });
        } else {
            $location.path("/login");
        }
    })


});


appControllers.controller('VerPedidosCtrl', function ($rootScope, $scope, ApiService){
    $scope.items = [];

    ApiService.pedidosRealizados()
        .success(function(items){
            $scope.items = items;
        })
        .error(function( data, status){
        })

    $rootScope.$on('app.ready', function(){
        ApiService.pedidosRealizados()
        .success(function(items){
            $scope.items = items;
        })
        .error(function( data, status){
        })
    })

});

/**!
 * AngularJS file upload/drop directive and service with progress and abort
 * FileAPI Flash shim for old browsers not supporting FormData
 * @author  Danial  <danial.farid@gmail.com>
 * @version 5.0.9
 */

(function () {
  /** @namespace FileAPI.noContentTimeout */

  function patchXHR(fnName, newFn) {
    window.XMLHttpRequest.prototype[fnName] = newFn(window.XMLHttpRequest.prototype[fnName]);
  }

  function redefineProp(xhr, prop, fn) {
    try {
      Object.defineProperty(xhr, prop, {get: fn});
    } catch (e) {/*ignore*/
    }
  }

  if (!window.FileAPI) {
    window.FileAPI = {};
  }

  FileAPI.shouldLoad = (window.XMLHttpRequest && !window.FormData) || FileAPI.forceLoad;
  if (FileAPI.shouldLoad) {
    var initializeUploadListener = function (xhr) {
      if (!xhr.__listeners) {
        if (!xhr.upload) xhr.upload = {};
        xhr.__listeners = [];
        var origAddEventListener = xhr.upload.addEventListener;
        xhr.upload.addEventListener = function (t, fn) {
          xhr.__listeners[t] = fn;
          if (origAddEventListener) origAddEventListener.apply(this, arguments);
        };
      }
    };

    patchXHR('open', function (orig) {
      return function (m, url, b) {
        initializeUploadListener(this);
        this.__url = url;
        try {
          orig.apply(this, [m, url, b]);
        } catch (e) {
          if (e.message.indexOf('Access is denied') > -1) {
            this.__origError = e;
            orig.apply(this, [m, '_fix_for_ie_crossdomain__', b]);
          }
        }
      };
    });

    patchXHR('getResponseHeader', function (orig) {
      return function (h) {
        return this.__fileApiXHR && this.__fileApiXHR.getResponseHeader ? this.__fileApiXHR.getResponseHeader(h) : (orig == null ? null : orig.apply(this, [h]));
      };
    });

    patchXHR('getAllResponseHeaders', function (orig) {
      return function () {
        return this.__fileApiXHR && this.__fileApiXHR.getAllResponseHeaders ? this.__fileApiXHR.getAllResponseHeaders() : (orig == null ? null : orig.apply(this));
      };
    });

    patchXHR('abort', function (orig) {
      return function () {
        return this.__fileApiXHR && this.__fileApiXHR.abort ? this.__fileApiXHR.abort() : (orig == null ? null : orig.apply(this));
      };
    });

    patchXHR('setRequestHeader', function (orig) {
      return function (header, value) {
        if (header === '__setXHR_') {
          initializeUploadListener(this);
          var val = value(this);
          // fix for angular < 1.2.0
          if (val instanceof Function) {
            val(this);
          }
        } else {
          this.__requestHeaders = this.__requestHeaders || {};
          this.__requestHeaders[header] = value;
          orig.apply(this, arguments);
        }
      };
    });

    patchXHR('send', function (orig) {
      return function () {
        var xhr = this;
        if (arguments[0] && arguments[0].__isFileAPIShim) {
          var formData = arguments[0];
          var config = {
            url: xhr.__url,
            jsonp: false, //removes the callback form param
            cache: true, //removes the ?fileapiXXX in the url
            complete: function (err, fileApiXHR) {
              xhr.__completed = true;
              if (!err && xhr.__listeners.load)
                xhr.__listeners.load({
                  type: 'load',
                  loaded: xhr.__loaded,
                  total: xhr.__total,
                  target: xhr,
                  lengthComputable: true
                });
              if (!err && xhr.__listeners.loadend)
                xhr.__listeners.loadend({
                  type: 'loadend',
                  loaded: xhr.__loaded,
                  total: xhr.__total,
                  target: xhr,
                  lengthComputable: true
                });
              if (err === 'abort' && xhr.__listeners.abort)
                xhr.__listeners.abort({
                  type: 'abort',
                  loaded: xhr.__loaded,
                  total: xhr.__total,
                  target: xhr,
                  lengthComputable: true
                });
              if (fileApiXHR.status !== undefined) redefineProp(xhr, 'status', function () {
                return (fileApiXHR.status === 0 && err && err !== 'abort') ? 500 : fileApiXHR.status;
              });
              if (fileApiXHR.statusText !== undefined) redefineProp(xhr, 'statusText', function () {
                return fileApiXHR.statusText;
              });
              redefineProp(xhr, 'readyState', function () {
                return 4;
              });
              if (fileApiXHR.response !== undefined) redefineProp(xhr, 'response', function () {
                return fileApiXHR.response;
              });
              var resp = fileApiXHR.responseText || (err && fileApiXHR.status === 0 && err !== 'abort' ? err : undefined);
              redefineProp(xhr, 'responseText', function () {
                return resp;
              });
              redefineProp(xhr, 'response', function () {
                return resp;
              });
              if (err) redefineProp(xhr, 'err', function () {
                return err;
              });
              xhr.__fileApiXHR = fileApiXHR;
              if (xhr.onreadystatechange) xhr.onreadystatechange();
              if (xhr.onload) xhr.onload();
            },
            progress: function (e) {
              e.target = xhr;
              if (xhr.__listeners.progress) xhr.__listeners.progress(e);
              xhr.__total = e.total;
              xhr.__loaded = e.loaded;
              if (e.total === e.loaded) {
                // fix flash issue that doesn't call complete if there is no response text from the server
                var _this = this;
                setTimeout(function () {
                  if (!xhr.__completed) {
                    xhr.getAllResponseHeaders = function () {
                    };
                    _this.complete(null, {status: 204, statusText: 'No Content'});
                  }
                }, FileAPI.noContentTimeout || 10000);
              }
            },
            headers: xhr.__requestHeaders
          };
          config.data = {};
          config.files = {};
          for (var i = 0; i < formData.data.length; i++) {
            var item = formData.data[i];
            if (item.val != null && item.val.name != null && item.val.size != null && item.val.type != null) {
              config.files[item.key] = item.val;
            } else {
              config.data[item.key] = item.val;
            }
          }

          setTimeout(function () {
            if (!FileAPI.hasFlash) {
              throw 'Adode Flash Player need to be installed. To check ahead use "FileAPI.hasFlash"';
            }
            xhr.__fileApiXHR = FileAPI.upload(config);
          }, 1);
        } else {
          if (this.__origError) {
            throw this.__origError;
          }
          orig.apply(xhr, arguments);
        }
      };
    });
    window.XMLHttpRequest.__isFileAPIShim = true;
    window.FormData = FormData = function () {
      return {
        append: function (key, val, name) {
          if (val.__isFileAPIBlobShim) {
            val = val.data[0];
          }
          this.data.push({
            key: key,
            val: val,
            name: name
          });
        },
        data: [],
        __isFileAPIShim: true
      };
    };

    window.Blob = Blob = function (b) {
      return {
        data: b,
        __isFileAPIBlobShim: true
      };
    };
  }

})();

(function () {
  /** @namespace FileAPI.forceLoad */
  /** @namespace window.FileAPI.jsUrl */
  /** @namespace window.FileAPI.jsPath */

  function isInputTypeFile(elem) {
    return elem[0].tagName.toLowerCase() === 'input' && elem.attr('type') && elem.attr('type').toLowerCase() === 'file';
  }

  function hasFlash() {
    try {
      var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
      if (fo) return true;
    } catch (e) {
      if (navigator.mimeTypes['application/x-shockwave-flash'] !== undefined) return true;
    }
    return false;
  }

  function getOffset(obj) {
    var left = 0, top = 0;

    if (window.jQuery) {
      return jQuery(obj).offset();
    }

    if (obj.offsetParent) {
      do {
        left += (obj.offsetLeft - obj.scrollLeft);
        top += (obj.offsetTop - obj.scrollTop);
        obj = obj.offsetParent;
      } while (obj);
    }
    return {
      left: left,
      top: top
    };
  }

  if (FileAPI.shouldLoad) {

    //load FileAPI
    if (FileAPI.forceLoad) {
      FileAPI.html5 = false;
    }

    if (!FileAPI.upload) {
      var jsUrl, basePath, script = document.createElement('script'), allScripts = document.getElementsByTagName('script'), i, index, src;
      if (window.FileAPI.jsUrl) {
        jsUrl = window.FileAPI.jsUrl;
      } else if (window.FileAPI.jsPath) {
        basePath = window.FileAPI.jsPath;
      } else {
        for (i = 0; i < allScripts.length; i++) {
          src = allScripts[i].src;
          index = src.search(/\/ng\-file\-upload[\-a-zA-z0-9\.]*\.js/);
          if (index > -1) {
            basePath = src.substring(0, index + 1);
            break;
          }
        }
      }

      if (FileAPI.staticPath == null) FileAPI.staticPath = basePath;
      script.setAttribute('src', jsUrl || basePath + 'FileAPI.min.js');
      document.getElementsByTagName('head')[0].appendChild(script);

      FileAPI.hasFlash = hasFlash();
    }

    FileAPI.ngfFixIE = function (elem, createFileElemFn, bindAttr, changeFn) {
      if (!hasFlash()) {
        throw 'Adode Flash Player need to be installed. To check ahead use "FileAPI.hasFlash"';
      }
      var makeFlashInput = function () {
        if (elem.attr('disabled')) {
          elem.$$ngfRefElem.removeClass('js-fileapi-wrapper');
        } else {
          var fileElem = elem.$$ngfRefElem;
          if (!fileElem) {
            fileElem = elem.$$ngfRefElem = createFileElemFn();
            fileElem.addClass('js-fileapi-wrapper');
            if (!isInputTypeFile(elem)) {
//						if (fileElem.parent().css('position') === '' || fileElem.parent().css('position') === 'static') {
//							fileElem.parent().css('position', 'relative');
//						}
//						elem.parent()[0].insertBefore(fileElem[0], elem[0]);
//						elem.css('overflow', 'hidden');
            }
            setTimeout(function () {
              fileElem.bind('mouseenter', makeFlashInput);
            }, 10);
            fileElem.bind('change', function (evt) {
              fileApiChangeFn.apply(this, [evt]);
              changeFn.apply(this, [evt]);
//						alert('change' +  evt);
            });
          } else {
            bindAttr(elem.$$ngfRefElem);
          }
          if (!isInputTypeFile(elem)) {
            fileElem.css('position', 'absolute')
              .css('top', getOffset(elem[0]).top + 'px').css('left', getOffset(elem[0]).left + 'px')
              .css('width', elem[0].offsetWidth + 'px').css('height', elem[0].offsetHeight + 'px')
              .css('filter', 'alpha(opacity=0)').css('display', elem.css('display'))
              .css('overflow', 'hidden').css('z-index', '900000')
              .css('visibility', 'visible');
          }
        }
      };

      elem.bind('mouseenter', makeFlashInput);

      var fileApiChangeFn = function (evt) {
        var files = FileAPI.getFiles(evt);
        //just a double check for #233
        for (var i = 0; i < files.length; i++) {
          if (files[i].size === undefined) files[i].size = 0;
          if (files[i].name === undefined) files[i].name = 'file';
          if (files[i].type === undefined) files[i].type = 'undefined';
        }
        if (!evt.target) {
          evt.target = {};
        }
        evt.target.files = files;
        // if evt.target.files is not writable use helper field
        if (evt.target.files !== files) {
          evt.__files_ = files;
        }
        (evt.__files_ || evt.target.files).item = function (i) {
          return (evt.__files_ || evt.target.files)[i] || null;
        };
      };
    };

    FileAPI.disableFileInput = function (elem, disable) {
      if (disable) {
        elem.removeClass('js-fileapi-wrapper');
      } else {
        elem.addClass('js-fileapi-wrapper');
      }
    };
  }
})();

if (!window.FileReader) {
  window.FileReader = function () {
    var _this = this, loadStarted = false;
    this.listeners = {};
    this.addEventListener = function (type, fn) {
      _this.listeners[type] = _this.listeners[type] || [];
      _this.listeners[type].push(fn);
    };
    this.removeEventListener = function (type, fn) {
      if (_this.listeners[type]) _this.listeners[type].splice(_this.listeners[type].indexOf(fn), 1);
    };
    this.dispatchEvent = function (evt) {
      var list = _this.listeners[evt.type];
      if (list) {
        for (var i = 0; i < list.length; i++) {
          list[i].call(_this, evt);
        }
      }
    };
    this.onabort = this.onerror = this.onload = this.onloadstart = this.onloadend = this.onprogress = null;

    var constructEvent = function (type, evt) {
      var e = {type: type, target: _this, loaded: evt.loaded, total: evt.total, error: evt.error};
      if (evt.result != null) e.target.result = evt.result;
      return e;
    };
    var listener = function (evt) {
      if (!loadStarted) {
        loadStarted = true;
        if (_this.onloadstart) _this.onloadstart(constructEvent('loadstart', evt));
      }
      var e;
      if (evt.type === 'load') {
        if (_this.onloadend) _this.onloadend(constructEvent('loadend', evt));
        e = constructEvent('load', evt);
        if (_this.onload) _this.onload(e);
        _this.dispatchEvent(e);
      } else if (evt.type === 'progress') {
        e = constructEvent('progress', evt);
        if (_this.onprogress) _this.onprogress(e);
        _this.dispatchEvent(e);
      } else {
        e = constructEvent('error', evt);
        if (_this.onerror) _this.onerror(e);
        _this.dispatchEvent(e);
      }
    };
    this.readAsArrayBuffer = function (file) {
      FileAPI.readAsBinaryString(file, listener);
    };
    this.readAsBinaryString = function (file) {
      FileAPI.readAsBinaryString(file, listener);
    };
    this.readAsDataURL = function (file) {
      FileAPI.readAsDataURL(file, listener);
    };
    this.readAsText = function (file) {
      FileAPI.readAsText(file, listener);
    };
  };
}

/**!
 * AngularJS file upload/drop directive and service with progress and abort
 * @author  Danial  <danial.farid@gmail.com>
 * @version 5.0.9
 */

if (window.XMLHttpRequest && !(window.FileAPI && FileAPI.shouldLoad)) {
  window.XMLHttpRequest.prototype.setRequestHeader = (function (orig) {
    return function (header, value) {
      if (header === '__setXHR_') {
        var val = value(this);
        // fix for angular < 1.2.0
        if (val instanceof Function) {
          val(this);
        }
      } else {
        orig.apply(this, arguments);
      }
    };
  })(window.XMLHttpRequest.prototype.setRequestHeader);
}

var ngFileUpload = angular.module('ngFileUpload', []);

ngFileUpload.version = '5.0.9';
ngFileUpload.service('Upload', ['$http', '$q', '$timeout', function ($http, $q, $timeout) {
  function sendHttp(config) {
    config.method = config.method || 'POST';
    config.headers = config.headers || {};

    var deferred = $q.defer();
    var promise = deferred.promise;

    config.headers.__setXHR_ = function () {
      return function (xhr) {
        if (!xhr) return;
        config.__XHR = xhr;
        if (config.xhrFn) config.xhrFn(xhr);
        xhr.upload.addEventListener('progress', function (e) {
          e.config = config;
          if (deferred.notify) {
            deferred.notify(e);
          } else if (promise.progressFunc) {
            $timeout(function () {
              promise.progressFunc(e);
            });
          }
        }, false);
        //fix for firefox not firing upload progress end, also IE8-9
        xhr.upload.addEventListener('load', function (e) {
          if (e.lengthComputable) {
            e.config = config;
            if (deferred.notify) {
              deferred.notify(e);
            } else if (promise.progressFunc) {
              $timeout(function () {
                promise.progressFunc(e);
              });
            }
          }
        }, false);
      };
    };

    $http(config).then(function (r) {
      deferred.resolve(r);
    }, function (e) {
      deferred.reject(e);
    }, function (n) {
      deferred.notify(n);
    });

    promise.success = function (fn) {
      promise.then(function (response) {
        fn(response.data, response.status, response.headers, config);
      });
      return promise;
    };

    promise.error = function (fn) {
      promise.then(null, function (response) {
        fn(response.data, response.status, response.headers, config);
      });
      return promise;
    };

    promise.progress = function (fn) {
      promise.progressFunc = fn;
      promise.then(null, null, function (update) {
        fn(update);
      });
      return promise;
    };
    promise.abort = function () {
      if (config.__XHR) {
        $timeout(function () {
          config.__XHR.abort();
        });
      }
      return promise;
    };
    promise.xhr = function (fn) {
      config.xhrFn = (function (origXhrFn) {
        return function () {
          if (origXhrFn) origXhrFn.apply(promise, arguments);
          fn.apply(promise, arguments);
        };
      })(config.xhrFn);
      return promise;
    };

    return promise;
  }

  this.upload = function (config) {
    function addFieldToFormData(formData, val, key) {
      if (val !== undefined) {
        if (angular.isDate(val)) {
          val = val.toISOString();
        }
        if (angular.isString(val)) {
          formData.append(key, val);
        } else if (config.sendFieldsAs === 'form') {
          if (angular.isObject(val)) {
            for (var k in val) {
              if (val.hasOwnProperty(k)) {
                addFieldToFormData(formData, val[k], key + '[' + k + ']');
              }
            }
          } else {
            formData.append(key, val);
          }
        } else {
          val = angular.isString(val) ? val : JSON.stringify(val);
          if (config.sendFieldsAs === 'json-blob') {
            formData.append(key, new Blob([val], {type: 'application/json'}));
          } else {
            formData.append(key, val);
          }
        }
      }
    }

    config.headers = config.headers || {};
    config.headers['Content-Type'] = undefined;
    config.transformRequest = config.transformRequest ?
      (angular.isArray(config.transformRequest) ?
        config.transformRequest : [config.transformRequest]) : [];
    config.transformRequest.push(function (data) {
      var formData = new FormData();
      var allFields = {};
      var key;
      for (key in config.fields) {
        if (config.fields.hasOwnProperty(key)) {
          allFields[key] = config.fields[key];
        }
      }
      if (data) allFields.data = data;
      for (key in allFields) {
        if (allFields.hasOwnProperty(key)) {
          var val = allFields[key];
          if (config.formDataAppender) {
            config.formDataAppender(formData, key, val);
          } else {
            addFieldToFormData(formData, val, key);
          }
        }
      }

      if (config.file != null) {
        var fileFormName = config.fileFormDataName || 'file';

        if (angular.isArray(config.file)) {
          var isFileFormNameString = angular.isString(fileFormName);
          for (var i = 0; i < config.file.length; i++) {
            formData.append(isFileFormNameString ? fileFormName : fileFormName[i], config.file[i],
              (config.fileName && config.fileName[i]) || config.file[i].name);
          }
        } else {
          formData.append(fileFormName, config.file, config.fileName || config.file.name);
        }
      }
      return formData;
    });

    return sendHttp(config);
  };

  this.http = function (config) {
    config.transformRequest = config.transformRequest || function (data) {
        if ((window.ArrayBuffer && data instanceof window.ArrayBuffer) || data instanceof Blob) {
          return data;
        }
        return $http.defaults.transformRequest[0](arguments);
      };
    return sendHttp(config);
  };
}

]);

(function () {
    ngFileUpload.directive('ngfSelect', ['$parse', '$timeout', '$compile',
        function ($parse, $timeout, $compile) {
            return {
                restrict: 'AEC',
                require: '?ngModel',
                link: function (scope, elem, attr, ngModel) {
                    linkFileSelect(scope, elem, attr, ngModel, $parse, $timeout, $compile);
                }
            };
        }]);

    function linkFileSelect(scope, elem, attr, ngModel, $parse, $timeout, $compile) {
        /** @namespace attr.ngfSelect */
        /** @namespace attr.ngfChange */
        /** @namespace attr.ngModel */
        /** @namespace attr.ngModelRejected */
        /** @namespace attr.ngfMultiple */
        /** @namespace attr.ngfCapture */
        /** @namespace attr.ngfAccept */
        /** @namespace attr.ngfMaxSize */
        /** @namespace attr.ngfMinSize */
        /** @namespace attr.ngfResetOnClick */
        /** @namespace attr.ngfResetModelOnClick */
        /** @namespace attr.ngfKeep */
        /** @namespace attr.ngfKeepDistinct */

        if (elem.attr('__ngf_gen__')) {
            return;
        }

        scope.$on('$destroy', function () {
            if (elem.$$ngfRefElem) elem.$$ngfRefElem.remove();
        });

        var disabled = false;
        if (attr.ngfSelect.search(/\W+$files\W+/) === -1) {
            scope.$watch(attr.ngfSelect, function (val) {
                disabled = val === false;
            });
        }
        function isInputTypeFile() {
            return elem[0].tagName.toLowerCase() === 'input' && attr.type && attr.type.toLowerCase() === 'file';
        }

        var isUpdating = false;

        function changeFn(evt) {
            if (!isUpdating) {
                isUpdating = true;
                try {
                    var fileList = evt.__files_ || (evt.target && evt.target.files);
                    var files = [], rejFiles = [];

                    for (var i = 0; i < fileList.length; i++) {
                        var file = fileList.item(i);
                        if (validate(scope, $parse, attr, file, evt)) {
                            files.push(file);
                        } else {
                            rejFiles.push(file);
                        }
                    }
                    updateModel($parse, $timeout, scope, ngModel, attr, attr.ngfChange || attr.ngfSelect, files, rejFiles, evt);
                    if (files.length === 0) evt.target.value = files;
//                if (evt.target && evt.target.getAttribute('__ngf_gen__')) {
//                    angular.element(evt.target).remove();
//                }
                } finally {
                    isUpdating = false;
                }
            }
        }

        function bindAttrToFileInput(fileElem) {
            if (attr.ngfMultiple) fileElem.attr('multiple', $parse(attr.ngfMultiple)(scope));
            if (attr.ngfCapture) fileElem.attr('capture', $parse(attr.ngfCapture)(scope));
            if (attr.accept) fileElem.attr('accept', attr.accept);
            for (var i = 0; i < elem[0].attributes.length; i++) {
                var attribute = elem[0].attributes[i];
                if ((isInputTypeFile() && attribute.name !== 'type') ||
                    (attribute.name !== 'type' && attribute.name !== 'class' &&
                    attribute.name !== 'id' && attribute.name !== 'style')) {
                    fileElem.attr(attribute.name, attribute.value);
                }
            }
        }

        function createFileInput(evt, resetOnClick) {
            if (!resetOnClick && (evt || isInputTypeFile())) return elem.$$ngfRefElem || elem;

            var fileElem = angular.element('<input type="file">');
            bindAttrToFileInput(fileElem);

            if (isInputTypeFile()) {
                elem.replaceWith(fileElem);
                elem = fileElem;
                fileElem.attr('__ngf_gen__', true);
                $compile(elem)(scope);
            } else {
                fileElem.css('visibility', 'hidden').css('position', 'absolute').css('overflow', 'hidden')
                    .css('width', '0px').css('height', '0px').css('z-index', '-100000').css('border', 'none')
                    .css('margin', '0px').css('padding', '0px').attr('tabindex', '-1');
                if (elem.$$ngfRefElem) {
                    elem.$$ngfRefElem.remove();
                }
                elem.$$ngfRefElem = fileElem;
                document.body.appendChild(fileElem[0]);
            }

            return fileElem;
        }

        function resetModel(evt) {
            updateModel($parse, $timeout, scope, ngModel, attr, attr.ngfChange || attr.ngfSelect, [], [], evt, true);
        }

        function clickHandler(evt) {
            if (elem.attr('disabled') || disabled) return false;
            if (evt != null) {
                evt.preventDefault();
                evt.stopPropagation();
            }
            var resetOnClick = $parse(attr.ngfResetOnClick)(scope) !== false;
            var fileElem = createFileInput(evt, resetOnClick);

            function clickAndAssign(evt) {
                if (evt) {
                    fileElem[0].click();
                }
                if (isInputTypeFile() || !evt) {
                    elem.bind('click touchend', clickHandler);
                }
            }

            if (fileElem) {
                if (!evt || resetOnClick) fileElem.bind('change', changeFn);
                if (evt && resetOnClick && $parse(attr.ngfResetModelOnClick)(scope) !== false) resetModel(evt);

                // fix for android native browser < 4.4
                if (shouldClickLater(navigator.userAgent)) {
                    setTimeout(function () {
                        clickAndAssign(evt);
                    }, 0);
                } else {
                    clickAndAssign(evt);
                }
            }
            return false;
        }

        if (window.FileAPI && window.FileAPI.ngfFixIE) {
            window.FileAPI.ngfFixIE(elem, createFileInput, bindAttrToFileInput, changeFn);
        } else {
            clickHandler();
            //if (!isInputTypeFile()) {
            //  elem.bind('click touchend', clickHandler);
            //}
        }
    }

    function shouldClickLater(ua) {
        // android below 4.4
        var m = ua.match(/Android[^\d]*(\d+)\.(\d+)/);
        if (m && m.length > 2) {
            return parseInt(m[1]) < 4 || (parseInt(m[1]) === 4 && parseInt(m[2]) < 4);
        }

        // safari on windows
        return /.*Windows.*Safari.*/.test(ua);
    }

    ngFileUpload.validate = function (scope, $parse, attr, file, evt) {
        function globStringToRegex(str) {
            if (str.length > 2 && str[0] === '/' && str[str.length - 1] === '/') {
                return str.substring(1, str.length - 1);
            }
            var split = str.split(','), result = '';
            if (split.length > 1) {
                for (var i = 0; i < split.length; i++) {
                    result += '(' + globStringToRegex(split[i]) + ')';
                    if (i < split.length - 1) {
                        result += '|';
                    }
                }
            } else {
                if (str.indexOf('.') === 0) {
                    str = '*' + str;
                }
                result = '^' + str.replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\' + '-]', 'g'), '\\$&') + '$';
                result = result.replace(/\\\*/g, '.*').replace(/\\\?/g, '.');
            }
            return result;
        }

        var accept = $parse(attr.ngfAccept)(scope, {$file: file, $event: evt});
        var fileSizeMax = $parse(attr.ngfMaxSize)(scope, {$file: file, $event: evt}) || 9007199254740991;
        var fileSizeMin = $parse(attr.ngfMinSize)(scope, {$file: file, $event: evt}) || -1;
        if (accept != null && angular.isString(accept)) {
            var regexp = new RegExp(globStringToRegex(accept), 'gi');
            accept = (file.type != null && regexp.test(file.type.toLowerCase())) ||
                (file.name != null && regexp.test(file.name.toLowerCase()));
        }
        return (accept == null || accept) && (file.size == null || (file.size < fileSizeMax && file.size > fileSizeMin));
    };

    ngFileUpload.updateModel = function ($parse, $timeout, scope, ngModel, attr, fileChange,
                                         files, rejFiles, evt, noDelay) {
        function update() {
            if ($parse(attr.ngfKeep)(scope) === true) {
                var prevFiles = (ngModel.$modelValue || []).slice(0);
                if (!files || !files.length) {
                    files = prevFiles;
                } else if ($parse(attr.ngfKeepDistinct)(scope) === true) {
                    var len = prevFiles.length;
                    for (var i = 0; i < files.length; i++) {
                        for (var j = 0; j < len; j++) {
                            if (files[i].name === prevFiles[j].name) break;
                        }
                        if (j === len) {
                            prevFiles.push(files[i]);
                        }
                    }
                    files = prevFiles;
                } else {
                    files = prevFiles.concat(files);
                }
            }
            if (ngModel) {
                $parse(attr.ngModel).assign(scope, files);
                $timeout(function () {
                    if (ngModel) {
                        ngModel.$setViewValue(files != null && files.length === 0 ? null : files);
                    }
                });
            }
            if (attr.ngModelRejected) {
                $parse(attr.ngModelRejected).assign(scope, rejFiles);
            }
            if (fileChange) {
                $parse(fileChange)(scope, {
                    $files: files,
                    $rejectedFiles: rejFiles,
                    $event: evt
                });
            }
        }

        if (noDelay) {
            update();
        } else {
            $timeout(function () {
                update();
            });
        }
    };

    var validate = ngFileUpload.validate;
    var updateModel = ngFileUpload.updateModel;

})();

(function () {
  var validate = ngFileUpload.validate;
  var updateModel = ngFileUpload.updateModel;

  ngFileUpload.directive('ngfDrop', ['$parse', '$timeout', '$location', function ($parse, $timeout, $location) {
    return {
      restrict: 'AEC',
      require: '?ngModel',
      link: function (scope, elem, attr, ngModel) {
        linkDrop(scope, elem, attr, ngModel, $parse, $timeout, $location);
      }
    };
  }]);

  ngFileUpload.directive('ngfNoFileDrop', function () {
    return function (scope, elem) {
      if (dropAvailable()) elem.css('display', 'none');
    };
  });

  ngFileUpload.directive('ngfDropAvailable', ['$parse', '$timeout', function ($parse, $timeout) {
    return function (scope, elem, attr) {
      if (dropAvailable()) {
        var fn = $parse(attr.ngfDropAvailable);
        $timeout(function () {
          fn(scope);
          if (fn.assign) {
            fn.assign(scope, true);
          }
        });
      }
    };
  }]);

  function linkDrop(scope, elem, attr, ngModel, $parse, $timeout, $location) {
    var available = dropAvailable();
    if (attr.dropAvailable) {
      $timeout(function () {
        if (scope[attr.dropAvailable]) {
          scope[attr.dropAvailable].value = available;
        } else {
          scope[attr.dropAvailable] = available;
        }
      });
    }
    if (!available) {
      if ($parse(attr.ngfHideOnDropNotAvailable)(scope) === true) {
        elem.css('display', 'none');
      }
      return;
    }

    var disabled = false;
    if (attr.ngfDrop.search(/\W+$files\W+/) === -1) {
      scope.$watch(attr.ngfDrop, function(val) {
        disabled = val === false;
      });
    }

    var leaveTimeout = null;
    var stopPropagation = $parse(attr.ngfStopPropagation);
    var dragOverDelay = 1;
    var actualDragOverClass;

    elem[0].addEventListener('dragover', function (evt) {
      if (elem.attr('disabled') || disabled) return;
      evt.preventDefault();
      if (stopPropagation(scope)) evt.stopPropagation();
      // handling dragover events from the Chrome download bar
      if (navigator.userAgent.indexOf('Chrome') > -1) {
        var b = evt.dataTransfer.effectAllowed;
        evt.dataTransfer.dropEffect = ('move' === b || 'linkMove' === b) ? 'move' : 'copy';
      }
      $timeout.cancel(leaveTimeout);
      if (!scope.actualDragOverClass) {
        actualDragOverClass = calculateDragOverClass(scope, attr, evt);
      }
      elem.addClass(actualDragOverClass);
    }, false);
    elem[0].addEventListener('dragenter', function (evt) {
      if (elem.attr('disabled') || disabled) return;
      evt.preventDefault();
      if (stopPropagation(scope)) evt.stopPropagation();
    }, false);
    elem[0].addEventListener('dragleave', function () {
      if (elem.attr('disabled') || disabled) return;
      leaveTimeout = $timeout(function () {
        elem.removeClass(actualDragOverClass);
        actualDragOverClass = null;
      }, dragOverDelay || 1);
    }, false);
    elem[0].addEventListener('drop', function (evt) {
      if (elem.attr('disabled') || disabled) return;
      evt.preventDefault();
      if (stopPropagation(scope)) evt.stopPropagation();
      elem.removeClass(actualDragOverClass);
      actualDragOverClass = null;
      extractFiles(evt, function (files, rejFiles) {
        updateModel($parse, $timeout, scope, ngModel, attr,
          attr.ngfChange || attr.ngfDrop, files, rejFiles, evt);
      }, $parse(attr.ngfAllowDir)(scope) !== false, attr.multiple || $parse(attr.ngfMultiple)(scope));
    }, false);

    function calculateDragOverClass(scope, attr, evt) {
      var accepted = true;
      var items = evt.dataTransfer.items;
      if (items != null) {
        for (var i = 0; i < items.length && accepted; i++) {
          accepted = accepted &&
            (items[i].kind === 'file' || items[i].kind === '') &&
            validate(scope, $parse, attr, items[i], evt);
        }
      }
      var clazz = $parse(attr.ngfDragOverClass)(scope, {$event: evt});
      if (clazz) {
        if (clazz.delay) dragOverDelay = clazz.delay;
        if (clazz.accept) clazz = accepted ? clazz.accept : clazz.reject;
      }
      return clazz || attr.ngfDragOverClass || 'dragover';
    }

    function extractFiles(evt, callback, allowDir, multiple) {
      var files = [], rejFiles = [], items = evt.dataTransfer.items, processing = 0;

      function addFile(file) {
        if (validate(scope, $parse, attr, file, evt)) {
          files.push(file);
        } else {
          rejFiles.push(file);
        }
      }

      function traverseFileTree(files, entry, path) {
        if (entry != null) {
          if (entry.isDirectory) {
            var filePath = (path || '') + entry.name;
            addFile({name: entry.name, type: 'directory', path: filePath});
            var dirReader = entry.createReader();
            var entries = [];
            processing++;
            var readEntries = function () {
              dirReader.readEntries(function (results) {
                try {
                  if (!results.length) {
                    for (var i = 0; i < entries.length; i++) {
                      traverseFileTree(files, entries[i], (path ? path : '') + entry.name + '/');
                    }
                    processing--;
                  } else {
                    entries = entries.concat(Array.prototype.slice.call(results || [], 0));
                    readEntries();
                  }
                } catch (e) {
                  processing--;
                  console.error(e);
                }
              }, function () {
                processing--;
              });
            };
            readEntries();
          } else {
            processing++;
            entry.file(function (file) {
              try {
                processing--;
                file.path = (path ? path : '') + file.name;
                addFile(file);
              } catch (e) {
                processing--;
                console.error(e);
              }
            }, function () {
              processing--;
            });
          }
        }
      }

      if (items && items.length > 0 && $location.protocol() !== 'file') {
        for (var i = 0; i < items.length; i++) {
          if (items[i].webkitGetAsEntry && items[i].webkitGetAsEntry() && items[i].webkitGetAsEntry().isDirectory) {
            var entry = items[i].webkitGetAsEntry();
            if (entry.isDirectory && !allowDir) {
              continue;
            }
            if (entry != null) {
              traverseFileTree(files, entry);
            }
          } else {
            var f = items[i].getAsFile();
            if (f != null) addFile(f);
          }
          if (!multiple && files.length > 0) break;
        }
      } else {
        var fileList = evt.dataTransfer.files;
        if (fileList != null) {
          for (var j = 0; j < fileList.length; j++) {
            addFile(fileList.item(j));
            if (!multiple && files.length > 0) {
              break;
            }
          }
        }
      }
      var delays = 0;
      (function waitForProcess(delay) {
        $timeout(function () {
          if (!processing) {
            if (!multiple && files.length > 1) {
              i = 0;
              while (files[i].type === 'directory') i++;
              files = [files[i]];
            }
            callback(files, rejFiles);
          } else {
            if (delays++ * 10 < 20 * 1000) {
              waitForProcess(10);
            }
          }
        }, delay || 0);
      })();
    }
  }

  ngFileUpload.directive('ngfSrc', ['$parse', '$timeout', function ($parse, $timeout) {
    return {
      restrict: 'AE',
      link: function (scope, elem, attr) {
        if (window.FileReader) {
          scope.$watch(attr.ngfSrc, function (file) {
            if (file &&
              validate(scope, $parse, attr, file, null) &&
              (!window.FileAPI || navigator.userAgent.indexOf('MSIE 8') === -1 || file.size < 20000) &&
              (!window.FileAPI || navigator.userAgent.indexOf('MSIE 9') === -1 || file.size < 4000000)) {
              $timeout(function () {
                //prefer URL.createObjectURL for handling refrences to files of all sizes
                //since it doesn´t build a large string in memory
                var URL = window.URL || window.webkitURL;
                if (URL && URL.createObjectURL) {
                  elem.attr('src', URL.createObjectURL(file));
                } else {
                  var fileReader = new FileReader();
                  fileReader.readAsDataURL(file);
                  fileReader.onload = function (e) {
                    $timeout(function () {
                      elem.attr('src', e.target.result);
                    });
                  };
                }
              });
            } else {
              elem.attr('src', attr.ngfDefaultSrc || '');
            }
          });
        }
      }
    };
  }]);

  function dropAvailable() {
    var div = document.createElement('div');
    return ('draggable' in div) && ('ondrop' in div);
  }

})();

(function(){
    'use strict';
    angular.module('taba.shortcuts',[]).provider('shortcuts',function(){
        this.$get = function ( $rootElement, $rootScope, $compile, $window, $document ) {
            
            Mousetrap.stopCallback = function( event, element ) {
                if( (' ' + element.className + ' ' ).indexOf(' mousetrap ') > -1 ) {
                    return false;
                }

                return ( element.contentEditable && element.contentEditable == 'true' );
            };

            function symbolize (combo) {
                var map = {
                  command   : '⌘',
                  shift     : '⇧',
                  left      : '←',
                  right     : '→',
                  up        : '↑',
                  down      : '↓',
                  'return'  : '↩',
                  backspace : '⌫'
                }; 

                combo = combo.split('+');

                for (var i = 0; i < combo.length; i++) {
                  // try to resolve command / ctrl based on OS:
                  if (combo[i] === 'mod') {
                    if ($window.navigator && $window.navigator.platform.indexOf('Mac') >=0 ) {
                      combo[i] = 'command';
                    } else {
                      combo[i] = 'ctrl';
                    }
                  }

                  combo[i] = map[combo[i]] || combo[i];
                }

                return combo.join(' + ');
            }

            function Shortcut (combo, description, callback, action, allowIn, persistent ) {
                
                this.combo = combo instanceof Array ? combo : [combo];
                this.description = description;
                this.callback = callback;
                this.action = action;
                this.allowIn = allowIn;
                this.persistent = persistent;
            }

            Shortcut.prototype.format = function() {
                var combo = this.combo[0];
                var sequence = combo.split(/[\s]/);
                for ( var i = 0; i < sequence.length; i++ ) {
                    sequence[i] = symbolize(sequence[i]);
                }
                return sequence;
            }

            // Nuevo Scope
            var scope = $rootScope.$new();

            scope.shortcuts = [];

            var boundScopes = [];

            $rootScope.$on('$routeChangeSuccess', function (event, route) {
                purgeShortcuts();

                if( route && route.shortcuts ) {
                    angular.forEach( route.shortcuts, function (shortcut) {
                        var callback = shortcut[2];
                        if( typeof(callback) === 'string' || callback instanceof String ) {
                            shortcut[2] = [callback, route];
                        }

                        // Definir como no persistente ya que es un shortcut asignado a una ruta
                        shortcut[5] = false;
                        _add.apply(this, shortcut);
                    })
                }
            });

            function purgeShortcuts() {
                var i = scope.shortcuts.length;
                while( i-- ) {
                    var shortcut = scope.shortcuts[i];
                    if( shortcut && !shortcut.persistent ) {
                        _del(shortcut);
                    }
                }
            }


            function _add (combo, description, callback, action, allowIn, persistent) {
                var _callback;

                var preventIn = ['INPUT', 'SELECT', 'TEXTAREA'];

                // Si se define como objeto setear los valores correspondientes
                var objType = Object.prototype.toString.call(combo);

                if( objType === '[object Object]') {
                    description = combo.description;
                    callback = combo.callback;
                    action = combo.action;
                    persistent = combo.persistent;
                    allowIn = combo.allowIn;
                    combo = combo.combo;
                }

                if (description instanceof Function) {
                  action = callback;
                  callback = description;
                  description = '$$undefined$$';
                } else if (angular.isUndefined(description)) {
                  description = '$$undefined$$';
                }

                if (persistent === undefined) {
                  persistent = true;
                }

                if (typeof callback === 'function' ) {
                    // guardar el callback original
                    _callback = callback;

                    // asegurarse que allowIn sea un array
                    if( ! (allowIn instanceof Array ) ) {
                        allowIn = [];
                    }

                    // Remover todo lo que este en preventIn que este dentro
                    // de allowIn
                    var index;
                    for ( var i = 0; i < allowIn.length; i++ ) {
                        allowIn[i]  = allowIn[i].toUpperCase();
                        index = preventIn.indexOf(allowIn[i]);
                        if( index !== -1 ) {
                            preventIn.splice(index, 1);
                        }
                    }

                    // wraper para el callback
                    callback = function (event) {
                        var shouldExecute = true;
                        var target = event.target || event.srcElement;
                        var nodeName = target.nodeName.toUpperCase();

                        if( (' ' + target.className + ' ').indexOf(' mousetrap ') > -1 ) {
                            shouldExecute = true;
                        } else {
                            for( var i = 0; i < preventIn.length; i++ ) {
                                if( preventIn[i] === nodeName ) {
                                    shouldExecute = false;
                                    break;
                                }
                            }
                        }

                        if( shouldExecute ) {
                            wrapApply( _callback.apply(this, arguments) );
                        }
                    }; 

                }

                if( typeof(action) === 'string') {
                    Mousetrap.bind(combo, wrapApply(callback), action);
                } else {
                    Mousetrap.bind(combo, wrapApply(callback));
                }

                var shortcut = new Shortcut (combo, description, callback, action, allowIn, persistent);
                scope.shortcuts.push(shortcut);
                return shortcut;
            }

            function _del (shortcut) {
                var combo = ( shortcut instanceof Shortcut ) ? shortcut.combo : shortcut;

                Mousetrap.unbind(combo);

                if( angular.isArray(combo) ) {
                    var retStatus = true;
                    var i = combo.length;
                    while( i-- ) {
                        retStatus = _del(combo[i]) && retStatus;
                    }
                    return retStatus;
                } else {
                    var index = scope.shortcuts.indexOf(_get(combo));

                    if( index > -1 ) {
                        if( scope.shortcuts[index].combo.length > 1 ) {
                            scope.shortcuts[index].combo.splice( scope.shortcuts[index].combo.indexOf(combo), 1);
                        } else {
                            scope.shortcuts.splice(index, 1);
                        }
                        return true;
                    }
                }

                return false;
            }

            function _get ( combo ) {
                var shortcut;

                for (var i = 0; i < scope.shortcuts.length; i++) {
                    var shortcut = scope.shortcuts[i];
                    
                    if( shortcut.combo.indexOf( combo) > -1 ) {
                        return shortcut;
                    }
                }

                return false;
            }


            function bindTo ( scope ) {
                
                if( ! (scope.$id in boundScopes ) ) {
                    boundScopes[scope.$id] = [];

                    scope.$on('$destroy', function(){
                        var i = boundScopes[scope.$id].length;
                        while( i-- ) {
                            _del(boundScopes[scope.$id].pop())
                        }
                    });
                }

                return {
                    add : function ( args ) {
                        var shortcut;

                        if( arguments.length > 1 ) {
                            shortcut = _add.apply(this, arguments);
                        } else {
                            shortcut = _add(args);
                        }

                        boundScopes[scope.$id].push(shortcut);

                        return this;
                    }
                }
            }

            function unbind( scope ) {
                if( (scope.$id in boundScopes ) ) {
                    var i = boundScopes[scope.$id].length;
                        while( i-- ) {
                            _del(boundScopes[scope.$id].pop())
                        }
                }
            }

            function wrapApply ( callback ) {
                return function (event, combo) {
                    if( callback instanceof Array ) {
                        var funcString = callback[0];
                        var route = callback[1];
                        callback = function( event ) {
                            route.scope.$eval(funcString);
                        }
                    }

                    $rootScope.$apply( function() {
                        callback( event, _get(combo) );
                    });
                }
            }

            var publicApi = {
                add : _add,
                del : _del,
                get : _get,
                bindTo : bindTo,
                purge : purgeShortcuts
            }

            return publicApi;
            
        }
    })
    .directive('shortcut', function( shortcuts ) {
        return {
            restrict : 'A',
            link : function( scope, el, attrs ) {
                var key, allowIn;

                angular.forEach(scope.$eval(attrs.shortcut), function (func, shortcut) {
                     allowIn = typeof attrs.shortcutAllowIn === "string" ? attrs.shortcutAllowIn.split(/[\s,]+/) : [];

                     key = shortcut;

                     shortcuts.add({
                        combo : shortcut,
                        description: attrs.shortcutDescription,
                        callback: func,
                        action : attrs.shortcutAction,
                        allowIn: allowIn
                     })
                });

                el.bind('$destroy', function() {
                    shortcuts.del(key);
                }); 
            }
        }
    })
    .run(function(shortcuts) {
    });

})();