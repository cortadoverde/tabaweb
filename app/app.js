
var app = angular.module('TabaApp', ['ngRoute', 'appControllers', 'appServices', 'appDirectives', 'taba.shortcuts', 'ngDialog']);

var appServices    = angular.module('appServices', []);
var appControllers = angular.module('appControllers', ['ngDialog','ngFileUpload']);
var appDirectives  = angular.module('appDirectives', []);

var options = {};
options.api = {};
options.api.base_url = "/api";

app.config(['$locationProvider', '$routeProvider',
  function($location, $routeProvider) {
    $routeProvider.

        when('/', {
            templateUrl: 'partials/ntavta.html',
            controller: 'PedidosCtrl',
            access: { requiredAuthentication: true }
        }).

        when('/login', {
            templateUrl: 'partials/user.signin.html',
            controller: 'AdminUserCtrl',
            show : { login: false }
        }).

        when('/logout', {
            templateUrl: 'partials/user.logout.html',
            controller: 'LogoutCtrl',
            access: { requiredAuthentication: true }
        }).
        when('/cuenta-corriente',{
            templateUrl: 'partials/cuenta-corriente.html',
            controller: 'CuentaCtrl',
            access : { requiredAuthentication : true }
        }).

        when('/ver-pedidos',{
            templateUrl: 'partials/pedidos_realizados.html',
            controller: 'VerPedidosCtrl',
            access : { requiredAuthentication : true }
        }).

        when('/disponibilidad',{
            templateUrl: 'partials/disponibilidad.html',
            controller: 'DisponibilidadCtrl',
            access : { requiredAuthentication : true }
        }).

        when('/archivos',{
            templateUrl: 'partials/descargas.html',
            controller: 'ArchivosCtrl',
            //access : { requiredAuthentication : true }
        }).

        when('/personalizar-codigos',{
            templateUrl: 'partials/personalizar-codigos.html',
            controller: 'CodigoClienteCtrl',
            access : { requiredAuthentication : true }
        }).

        otherwise({
            redirectTo: '/'
        });
}]);


app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
    $httpProvider.interceptors.push('StatusInterceptor');
});

app.run(function($rootScope, $location, $window, AuthenticationService, $http, UserService) {
    $rootScope.taba = {
      error : false,
      state : {
        loading: false,
        error: false
      },
      api   : options.api.base_url.replace('http://', '')
    }
    $rootScope.mode = {
        display: false,
        search: true
    }

    $rootScope.changeMode = function( type ) {
        if( type == 'display' ) {
            $rootScope.mode.display = true;
            $rootScope.mode.search = false;
        } else {
            $rootScope.mode.display = false;
            $rootScope.mode.search = true;
        }
    }

    $rootScope.loadInt = 0;

    $rootScope.AppReady = false;

    function logOut() {
        $rootScope.changeMode('search');
        UserService.logOut().success(function(data) {
            AuthenticationService.isAuthenticated = false;
            $rootScope.auth = {};
            $rootScope.auth.logged = false;
            delete $window.localStorage.token;
            $rootScope.$broadcast('auth.change')
        }).error(function(status, data) {
            delete $window.localStorage.token;
            $rootScope.$broadcast('auth.change')
        });
    }

    $rootScope.logOut = logOut;

    $rootScope.empresa = {};

    $http.get(options.api.base_url + '/about').success(function(data){
      $rootScope.empresa = data;
      $rootScope.$broadcast('empesa.ready');
    })

    if( $window.localStorage.token ) {
        UserService.me(function(data){
            AuthenticationService.isAuthenticated = true;
            $rootScope.$broadcast('auth.change')
        }, function(){
            $rootScope.$broadcast('auth.change')
        })

    }

    $rootScope.auth = {
        logged : false
    };

    console.log($rootScope.auth)

    $rootScope.$on('$routeChangeSuccess', function( event, currentRoute ) {
      if( currentRoute.$$route && currentRoute.$$route.controller ) {
        $rootScope.$broadcast('route.' + currentRoute.$$route.controller );
        console.log(currentRoute.$$route.controller );
      }
    })

    $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {

        if( nextRoute.$$route && nextRoute.$$route.templateUrl && nextRoute.$$route.templateUrl == 'partials/user.logout.html' ){
            logOut();
            $location.path("/login");
        }


        //redirect only if both isAuthenticated is false and no token is set
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
            && !AuthenticationService.isAuthenticated && !$window.localStorage.token) {
            $location.path("/login");
        }
        // redireccionar a la home si no ya esta logueado
        if( nextRoute != null && nextRoute.show != null && !nextRoute.show.login && !AuthenticationService.isAuthenticated && $window.localStorage.token ) {
          $location.path("/");
        }

    });


    // Eventos para indicar el estado de carga
    $rootScope.$on('http.request', function(){
      $rootScope.taba.state.loading = true;
    });

    $rootScope.$on('http.requestError', function(){
      $rootScope.taba.state.loading = false;
    })

    $rootScope.$on('http.response', function(){
      $rootScope.taba.state.loading = false;
    })

    $rootScope.$on('http.responseError', function(){
      $rootScope.taba.state.loading = false;
    })



});


app.filter('procesoEstado', function() {
    return function( estado ) {
        if( estado == 2 ) {
            return 'En proceso';
        }
        return 'completado';
    }
})

app.filter('procesoEstadoColor', function() {
    return function( estado ) {
        if( estado == 2 ) {
            return 'orange';
        }
        return 'green';
    }
})

app.filter('debe', function(){
    return function( value ) {
        if( value >= 0 ) {
            return value;
        }
        return null;
    }
})

app.filter('haber', function(){
    return function( value ) {
        if( value < 0 ) {
            return value * -1 ;
        }
        return null;
    }
});

app.filter('fileSize', function(){
  return function( bytes ) {
    var thresh = 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = ['kB','MB','GB','TB','PB','EB','ZB','YB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);

    return bytes.toFixed(1)+' '+units[u];
  }
})
app.filter('fileTypeIcon', function(){
  return function ( fileName ) {
    var ext = fileName.split('.').pop();
    var icons = {
      'pdf' : 'file pdf outline',
      'xls' : 'file excel outline',
      'xlsx' : 'file excel outline',
      'doc' : 'file word outline',
      'docx' : 'file word outline',
      'txt' : 'file text outline',
      'zip' : 'file archive outline',
      'rar' : 'file archive outline',
      'tar' : 'file archive outline',
      'gz' : 'file archive outline',
      '7z' : 'file archive outline',
      'jpg' : 'file image outline',
      'png' : 'file image outline',
      'gif' : 'file image outline',
      'jpeg' : 'file image outline',
      '_def' : 'file'
    };

    if( ! icons[ext] ) {
      return icons['_def'];
    }
    return icons[ext];

  }
})
