appControllers.controller('AdminUserCtrl', ['$scope', '$location', '$window', 'UserService', 'AuthenticationService', '$rootScope',
    function AdminUserCtrl($scope, $location, $window, UserService, AuthenticationService, $rootScope) {

        //Admin User Controller (signIn, logOut)
        $scope.signIn = function signIn(username, password) {
            if (username != null && password != null) {

                var tipo = $scope.user.es_vendedor ? 'vendedor' : 'cliente';

                UserService.signIn(username, password, tipo).success(function(data) {
                    AuthenticationService.isAuthenticated = true;
                    $rootScope.auth.logged = true;
                    UserService.curretUser = data;
                    $window.localStorage.token = data.token;
                    $rootScope.$broadcast('auth.change');
                    //$window.open('/','myInstance','fullscreen=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no')
                }).error(function(data, status) {
                    if( typeof data.level !== 'undefined' ) {
                      $rootScope.taba.error = {
                       level : data.level,
                       content: data.error
                      };
                      setTimeout(function(){
                          $rootScope.taba.error = null;
                          $rootScope.$apply();
                      }, 3000);
                    }
                    $rootScope.$broadcast('auth.change')
                });

            }
        }


    }
]);

appControllers.controller('LogoutCtrl',function ($scope){

})
