/**
 * Controlador de Pedidos
 * Gestiona las solicitudes de los clientes, interactua con el servicio: @ApiService
 * que envia request a la url de la API del Taba
 */
appControllers.controller('PedidosCtrl', function ($scope, ApiService, $rootScope, ngDialog, Upload) {
    // Pendientes a procesar, se cargara con los datos obtenidos de la api
    // esta variable esta constantemente actualizandose
    $scope.pendings = [];

    // resultados de busqueda
    $scope.resultados = [];

    // Pedido actual
    $scope.order = {};

    // Variable de control de muestra precio,
    // a futuro debera validarse desde opciones de un panel de administracion
    $scope.showPrice =  false;

    // Variable de control para saber si en la etiqueta de color indica la cantidad
    // disponible de un producto, esta se activa actualmente si el usuario es vendedor
    $scope.showCantidad = false;

    // Variable de control para el importador para saber si los checkbox de cada fila
    // se activan o no
    $scope.all = true;

    // Notificaciones globales
    // formato
    // {
    //   * type : [danger|warning|info],
    //   * body : String,
    //   icon   : [icono],
    //   header : String
    // }
    $scope.notificaciones = [];

    // Errores De importacion
    // formato
    // {
    //   codProducto
    //   message
    // }
    $scope.errores_importacion = [];

    // Control de errores de salida, para mantener una integridad
    var errorCodes = {
      1 : 'No se encontraro registros',
      2 : 'La "cantidad" debe ser un número'
    }

    // Metodo para procesar la respuesta del callback de la API de busqueda de productos, ya sea por codigo
    // de producto o por codigo de cliente
    function processSearchResult( items ) {
      $scope.notificaciones = [];
      if( items.length == 0 ) {
        $scope.notificaciones.push({
          _errorCode : 1,
          type : 'warning',
          icon : 'warning',
          header: errorCodes[1],
          body: 'Verifique el código de producto y vuelva a intentarlo, busqueda actual: ' + $scope.search
        })
      } else {
        _.remove($scope.notificaciones, { _errorCode : 1 } )
      }
      $scope.resultados = items;
    }

    // funcion de comprobacion para las variables de control
    var chk = function() {
        // $scope.showPrice = $rootScope.auth._type == 'vendedor' ? true : $rootScope.empresa.verPrecio;
        $scope.showCantidad = $rootScope.auth._type == 'vendedor' ? true : $rootScope.empresa.verCantidad;
    }

    // Evento principal
    // se dispara cuando la aplicacion se ha terminado de cargar
    $rootScope.$on('app.ready', function(){
        chk();
        $scope.getPendings();
    })

    // Inicialmente busca pedidos pendientes
    ApiService.pedidosPendientes()
      .success(function(items){
          chk();
          $scope.pendings = items;
      })
      .error(function( data, status){
      })

    // Elimina la notificaion al pulsar en la cruz
    $scope.removeNotification = function( idx ) {
      $scope.notificaciones.splice(idx, 1);
    }

    // Elimina el mensaje de error
    $scope.removeError = function( idx ) {
      $scope.errores_importacion.splice(idx, 1);
    }

    // Hace un request a la API y obtiene los
    // pedidos que no se enviaron a procesar todavia
    $scope.getPendings = function() {

        $scope.search = '';

        ApiService.pedidosPendientes()
        .success(function(items){
            $scope.pendings = items;
        })
        .error(function( data, status){
            $scope.pendings = [];
        })
    }

    // Confirma el pedido pendiente y pasa a pedidos realizados
    $scope.confirm = function() {
        ApiService.pedidoConfirmar()
            .success(function(data){
                $scope.getPendings();
            })
    }

    // Elimina todos los pedidos pendientes
    $scope.deleteAll = function(e) {
        e.preventDefault()
        if( confirm(' Seguro que desea eliminar este registro ?') ) {
            ApiService.pedidosCancelar()
                .success(function(data){
                    $scope.getPendings();
                })
        }
    }

    // Elimina un pendiente de la lista actual
    $scope.deleteItem = function( registro ) {
        if( confirm(' Seguro que desea eliminar este registro ?') ) {
            ApiService.pedidosEliminarRegistro( registro )
                .success(function(){
                    $scope.getPendings();
                })
        }
    }

    // Envia una peticion a la API para buscar productos por codigo de producto
    $scope.buscarCodigoProducto = function() {
        ApiService.buscarProductoPorCodigo( $scope.search )
            .success(processSearchResult)
    }

    // Similar a buscarCodigoProducto pero con el codigo de cliente
    $scope.buscarCodigoCliente = function() {
        ApiService.buscarProductoPorCodigoCliente( $scope.search )
            .success(processSearchResult)
    }

    // Crea un pedido pendiente con los datos de seleccion
    // de la busqueda de productos
    $scope.crearPedido = function() {
       ApiService.crearPedido( $scope.order )
          .success(function( data ) {
              $scope.resultados = [];
              $scope.order = {};
              $scope.getPendings();
          })
    }

    // Evalua el evento keyup para el input del buscador
    $scope.evalKey = function( event ) {
        if( event.keyCode && event.keyCode == '13' ) {
            $scope.buscarCodigoProducto();
        }
    }

    $scope.evalKeyItem = function( $event, validation ) {
        console.log(validation);
        if( validation && validation == 'number' ) {
          if(!(($event.keyCode > 95 && $event.keyCode < 106)
            || ($event.keyCode > 47 && $event.keyCode < 58)
            || $event.keyCode == 8)) {
              $event.preventDefault();
              return false;
          }
        }

        if( $event.keyCode && $event.keyCode == '13' ) {
            $scope.crearPedido();
        }
    }

    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: options.api.base_url + '/upload',
                    fields: {'username': 'tabauploader'},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    $scope.showContent(data);
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                }).error(function (data, status, headers, config) {
                    console.log('error status: ' + status);
                })
            }
        }
    };

    $scope.importarExcel = function() {
        var o     = {};
        var error = [];
        var n     = 0;
        $scope.errores_importacion = [];

        var colCodProducto = _.findKey( $scope.columna, function( str ) { return str == 'codProducto' } ).toUpperCase();
        var colCantidad    = _.findKey( $scope.columna, function( str ) { return str == 'cantidad' } ).toUpperCase();
        var colDetalle     = _.findKey( $scope.columna, function( str ) { return str == 'detalle' } ).toUpperCase();

        _.forEach($scope.cellOrder, function(obj, index) {
            if( obj.import ) {
                n++;

                if( typeof obj[colCodProducto] == 'undefined' ) return;

                var codProducto = obj[colCodProducto].toString().toUpperCase();
                var cantidad = obj[colCantidad] ;
                var detalle  = obj[colDetalle];

                if( _.trim(codProducto).length == 0 ) return;

                if( isNaN( cantidad ) ) {
                    cantidad = 0
                }
                cantidad = parseInt( cantidad );

                if( ! o.hasOwnProperty( codProducto ) ) {
                    o[codProducto] = {
                        cantidad: cantidad,
                        detalle : detalle
                    }
                } else {
                    o[codProducto].cantidad = parseInt(o[codProducto].cantidad) + parseInt(cantidad);
                   // o[codProducto].detalle = o[codProducto].detalle + ' / ' + detalle;
                }

                if( o[codProducto].cantidad < 1 )
                  delete o[codProducto];

            }

        })

        if( error.length > 0 ) {
          $scope.notificaciones = _.extend($scope.notificaciones, error);
        }

        if( error.length == n ) {
          $scope.dialog.close();
          return false;
        }



        ApiService.crearPedido( o )
            .success(function( data ) {
                $scope.getPendings();
                console.log(data);
                if( typeof data.error !== "undefined" && data.error.length > 0 )
                  $scope.errores_importacion = data.error;
                $scope.dialog.close()
            })
    }

    $scope.checkAll = function() {
        console.log( $scope.all );
        if( $scope.all ) {
            $scope.all = false;
        } else {
            $scope.all = true;
        }


        _.forEach($scope.cellOrder,function(n, idx){
            $scope.cellOrder[idx].import = $scope.all;
        });
    }

    $scope.clearNotificaciones = function() {
      $scope.notificaciones = [];
      $scope.errores_importacion = [];
    }


    $scope.showContent = function(result){

        $scope.checkAllItemes = true;


        $scope.columna = {
            a : 'codProducto',
            b : 'cantidad',
            c : 'detalle'
        };

        $scope.cellOrder = {};

        $scope.cells = result;

        $scope.dialog = ngDialog.open({
            template : 'partials/import_excel.html',
            className: 'ngdialog-theme-default',
            scope : $scope
        })

    };
});
