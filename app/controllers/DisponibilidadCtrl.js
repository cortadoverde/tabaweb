
appControllers.controller('DisponibilidadCtrl', function ( $rootScope, $scope, ApiService){
    $scope.items = [];
    $scope.search = '';
    $scope.showPrice =  false;
    $scope.showCantidad = false;

    // Notificaciones globales
    // formato
    // {
    //   * type : [danger|warning|info],
    //   * body : String,
    //   icon   : [icono],
    //   header : String
    // }
    $scope.notificaciones = [];

    // Control de errores de salida, para mantener una integridad
    var errorCodes = {
      1 : 'No se encontraro registros'
    }

    var chk = function() {
        // $scope.showPrice = $rootScope.auth._type == 'vendedor' ? true : $rootScope.empresa.verPrecio;
        $scope.showCantidad = $rootScope.auth._type == 'vendedor' ? true : $rootScope.empresa.verCantidad;
    }

    function processSearchResult( items ) {
      if( items.length == 0 ) {
        $scope.notificaciones.push({
          _errorCode : 1,
          type : 'warning',
          icon : 'warning',
          header: errorCodes[1],
          body: 'Verifique el código de producto y vuelva a intentarlo, busqueda actual: ' + $scope.search
        })
      } else {
        _.remove($scope.notificaciones, { _errorCode : 1 } )
      }
      $scope.items = items;
    }

    chk();

    $scope.$on('$viewContentLoaded', function(){
      chk();
      console.log('trigger $viewContentLoaded')
    })
    $rootScope.$on('route.DisponibilidadCtrl',function(){
      chk();
      console.log('trigger route.DisponibilidadCtrl')
    })

    $rootScope.$on('app.ready', function(){
        chk();
        console.log('trigger app ready')
    })


    $scope.buscarCodigoProducto = function() {
        ApiService.stock('codProducto', $scope.search )
            .success(processSearchResult)
    }

    $scope.buscarCodigoCliente = function() {
        ApiService.stock('codCliente', $scope.search )
            .success(processSearchResult)
    }

    $scope.getDisplay = function( item ) {
        if( item.stockactual1 > item.stockminimo ) {
            return '<b>disponible</b>';
        } else {
            if( item.stockactual1  > 0 ) {
                return '<b>consultar</b>';
            } else {
                return '<b>No disponible</b>';
            }
        }
    }

    $scope.evalKey = function( event ) {
        if( event.keyCode && event.keyCode == '13' ) {
            $scope.buscarCodigoProducto();
        }
    }
});
