
appControllers.controller('VerPedidosCtrl', function ($rootScope, $scope, ApiService){
    $scope.items = [];

    ApiService.pedidosRealizados()
        .success(function(items){
            $scope.items = items;
        })
        .error(function( data, status){
        })

    $rootScope.$on('app.ready', function(){
        ApiService.pedidosRealizados()
        .success(function(items){
            $scope.items = items;
        })
        .error(function( data, status){
        })
    })

});
