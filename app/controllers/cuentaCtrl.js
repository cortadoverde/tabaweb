
appControllers.controller('CuentaCtrl', function($scope, $rootScope, ApiService, ngDialog){
    $scope.pendings = [];
    $scope.resumen = [];
    $scope.state = 'pending';
    $scope.switchTo = 'resumen';

    $scope._load = function() {
        ApiService.cuenta($scope.state)
            .success(function( data ){
                if( $scope.state == 'pending' ) {
                    $scope.pendings = data;
                } else {
                    $scope.resumenSaldo = data.SaldoInicial;
                    $scope.resumen = data.Items;
                }
            })
    }

    $scope.$watch('state', function(n, o){
        if( n == 'pending' ) {
            $scope.resumen = [];
            $scope.resumenSaldo = 0;
            $scope.switchTo = 'Resumen de cuenta corriente';
        } else {
            $scope.pendings = [];
            $scope.switchTo = 'Pendientes por cobrar';
        }
        $scope._load();
    })

    $rootScope.$on('app.ready', function(){
        $scope._load();
    })

    $scope.changeState = function() {
        $scope.state = $scope.state == 'pending' ? 'resumen' : 'pending';
    }

    $scope.calcSaldo = function( index ) {
       var totales =  _.pluck( $scope.pendings, 'Saldo' ) ;
       return _.sum(totales.slice(0, index + 1 ));
    }

    $scope.calcSaldoResumen = function( index ) {
       var totales =  _.pluck( $scope.resumen, 'ImporteCbte' ) ;
       return  $scope.resumenSaldo + _.sum(totales.slice(0, index + 1 ));
    }

    $scope.ver_comprobante = function(event, tipoId, numeroId ) {
        event.preventDefault();

        ApiService.comprobante(tipoId, numeroId)
            .success(function( comprobante ){
                $scope.factura = comprobante;
                ngDialog.open({
                    template : 'partials/comprobante.html',
                    className: 'ngdialog-theme-default',
                    scope : $scope
                })
            })
    }




});
