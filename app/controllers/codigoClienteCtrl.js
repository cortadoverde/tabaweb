
appControllers.controller('CodigoClienteCtrl', function ( $rootScope, $scope, ApiService, ngDialog, Upload){
    $scope.code = {};

    $scope.buscarCodigoProducto = function() {
        ApiService.buscarProductoPorCodigo( $scope.search )
            .success(function( items ) {
                $scope.resultados = items;
            })
    }

    $scope.crearCodigo = function() {

        ApiService.crearCodigo( $scope.code )
            .success(function( ) {
                $scope.resultados = [];
            })
    }

    $scope.evalKey = function( $event ) {
        if( $event.keyCode && $event.keyCode == '13' ) {
            $scope.buscarCodigoProducto();
        }
    }

    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: options.api.base_url + '/upload',
                    fields: {'username': 'tabauploader'},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    $scope.showContent(data);
                }).error(function (data, status, headers, config) {
                    console.log('error status: ' + status);
                })
            }
        }
    };


    $scope.showContent = function(result){

        $scope.checkAllItemes = true;

        $scope.columna = {
            a : 'codigoProducto',
            b : 'codigoCliente',
            c : 'nombre'
        };

        $scope.cellOrder = {};

        $scope.cells = result;

        $scope.dialog = ngDialog.open({
            template : 'partials/import_excel_codigo.html',
            className: 'ngdialog-theme-default',
            scope : $scope
        })

    };

    $scope.importarExcel = function() {
        var o = {};
        var error = [];

        console.log( $scope.columna );
        var colCodProducto = _.findKey( $scope.columna, function( str ) { return str == 'codigoProducto' } ).toUpperCase();
        var colCodCliente  = _.findKey( $scope.columna, function( str ) { return str == 'codigoCliente' } ).toUpperCase();
        var colDetalle     = _.findKey( $scope.columna, function( str ) { return str == 'nombre' } ).toUpperCase();

        _.forEach($scope.cellOrder, function(obj, index) {
            if( obj.import ) {
                var codProducto = obj[colCodProducto].toString().toUpperCase();
                var codCliente  = obj[colCodCliente];
                var detalle     = obj[colDetalle];

                if( ! o.hasOwnProperty( codProducto ) ) {
                    o[codProducto] = {
                        codigo: codCliente,
                        nombre : detalle
                    }
                }
            }
        });

        if( ! _.isEmpty( o ) &&  error.length == 0 ) {
            ApiService.crearCodigo( o )
                .success(function( ) {
                    $scope.dialog.close();
                    $scope.buscarCodigoProducto();
                })
        } else {
            alert( error.join('\n') );
        }
    }

    $scope.checkAll = function() {
        console.log( $scope.all );
        if( $scope.all ) {
            $scope.all = false;
        } else {
            $scope.all = true;
        }

        console.log($scope.checkAllItemes);
        console.log($scope);

        _.forEach($scope.cellOrder,function(n, idx){
            $scope.cellOrder[idx].import = $scope.all;
        });
    }

});
