appControllers.controller('findUserCtrl', function ($scope, $rootScope, ApiService, $timeout, $location) {

    $scope.list = [];
    $scope.selected = false;
    $scope.search = '';
    $scope.focusInput = true;

    $scope.reset = function() {
        $scope.list = [];
        $scope.selected = false;
        $scope.search = '';
        $scope.focusInput = true;
    }

    $rootScope.$on('auth.change', function(){
        $scope.reset();
    })

    $scope.find = function ( str ) {
        ApiService.findCuenta(str).success(function (data, status) {
            $scope.list = data;
        })
    }

    $scope.selectElm = function( item ) {
        $scope.selected = item;
        //$scope.search = item.Cuenta ;
        $rootScope.mode.search = false;
        $rootScope.mode.display = true;
        $scope.focusInput = false;
        ApiService.setCuenta(item.Cuenta).success(function (data, status) {
            $rootScope.auth._current = data;
            // console
            console.log($location.path());
            $rootScope.$broadcast('app.ready');
        })
    }

    $scope.keydown = function( event ) {
        $scope.selected=false;
        if( event.keyCode == 27 ) {
            $scope.list = [];
            $rootScope.changeMode('display');
        }
        if( event.keyCode == 9 ) {
            $('.searchResults').focus();
            event.preventDefault();
        }
    }

    $scope.selectByEnter = function( event, item) {
        if( event.keyCode == 13 ) {
            $scope.selectElm(item)
        }
    }

    $scope.editMode = function() {
        if( $rootScope.auth._type == 'vendedor' ) {
            $rootScope.changeMode('search');
            $scope.focusInput = true;
        }
        //test
    }

    $scope.$watch('search', function(str){
        if( str.length > 2 )
            $scope.find(str);
        else
            $scope.list = []
    })


});
