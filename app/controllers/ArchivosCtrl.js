appControllers.controller('ArchivosCtrl', function ( $rootScope, $scope, ApiService, ngDialog, Upload, $window ){
  $scope.groups = null;
  $scope._actives = {};

  ApiService.archivos()
      .success(function(items){
          $scope.groups = items;
          console.log(items);
      })
      .error(function( data, status){
      })


  $scope.download = function( fileData ) {
    $window.location.href = options.api.base_url + '/download/' + fileData._id;
    //$location.replace();
  }

  $scope.remove_file = function( file_id, group_id ) {
    var updateGroupId = _.findIndex( $scope.groups, function( group ) {
      return group._id == group_id;
    });

    ApiService.removerArchivo(file_id)
      .success(function( data ){
          $scope.groups[updateGroupId] = data;
      })
  }

  $scope.createGroup = function() {

    $scope.abm_grupo = {
      _action : "Crear nuevo "

    }

    $scope.dialog = ngDialog.open({
        template : 'partials/createGroup.html',
        className: 'ngdialog-theme-default',
        scope : $scope
    })
  }

  $scope.saveGroup = function() {
      ApiService.createGrupo($scope.abm_grupo)
        .success(function( response ) {
          $scope.groups.push(response);
        })
      $scope.dialog.close();
  }

  $scope.deleteGrupo = function( grupo )
  {
    $scope.groups = _.without($scope.groups,grupo)
  }

  $scope.active_upload = function( group_id ) {
    return typeof $scope._actives[group_id]  !== "undefined";
  }

  $scope.progress = function( group_id ) {
    return $scope._actives[group_id];
  }
  $scope.upload = function (files, grupo) {
      $scope._actives[grupo] = true;
      var updateGroupId = _.findIndex( $scope.groups, function( group ) {
        return group._id == grupo;
      });


      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          var UploadHandler = Upload.upload({
            url: options.api.base_url + '/descargas/upload',
            file: file,
            fields : {
              group_id : grupo
            }
          });

          UploadHandler.progress( function (evt) {
            $scope._actives[grupo] = parseInt(100.0 * evt.loaded / evt.total);
          })

          UploadHandler.success( function (data, status, headers, config) {
            $scope.groups[updateGroupId] = data;
            delete $scope._actives[grupo];

          })

          UploadHandler.error( function() {
          })

        }
      }
      /*
      if (files && files.length) {
          for (var i = 0; i < files.length; i++) {
              var file = files[i];
              Upload.upload({
                  url: options.api.base_url + '/descargas/upload',
                  file: file
              }).progress(function (evt) {
                  var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                  console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
              }).success(function (data, status, headers, config) {
                  $scope.showContent(data);
                  console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
              }).error(function (data, status, headers, config) {
                  console.log('error status: ' + status);
              })
          }
      }
      */
  };

});
