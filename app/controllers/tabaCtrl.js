
appControllers.controller('tabaCtrl', function ($scope, $rootScope, AuthenticationService, UserService, $location, shortcuts){

    $scope.taba = $scope.taba || {};
    $scope.taba.menu = [];

    var menu = [
       // scope.taba.menu = [
        {
            url : 'realizar-pedidos',
            title: 'Nuevo Pedido',
            perms: 2,
            icon: 'file text outline'
        },
        {
            url: 'ver-pedidos',
            title: 'Pedidos realizados',
            perms : 2,
            icon: 'archive'
        },
        {
            url : 'disponibilidad',
            title : 'Disponibilidad',
            perms: 1,
            icon: 'cubes'
        },
        {
            url: 'archivos',
            title: 'Descargas',
            perms: 0,
            icon: 'cloud download'
        },
        {
            url : 'cuenta-corriente',
            title: 'Cuenta Corriente',
            perms: 1,
            icon: 'money'
        },
        {
            url : 'personalizar-codigos',
            title: 'Código Cliente',
            perms: 2,
            icon: 'barcode'
        },
        {
            url : 'logout',
            title: 'Salir',
            perms: 0,
            align: 'right',
            icon: 'sign out',
            action: 'ng-click="userLogout()"'
        }
    ];

    $rootScope.$on('apprun', function(){
    })

    $rootScope.$on('app.ready', function(){
        $rootScope.$broadcast('taba.menu');
    })

    $rootScope.$on('taba.menu', function() {
        var redirect;
        $scope.taba.menu = [];
        if( $rootScope.auth.logged ) {
            angular.forEach(menu, function(item){
                if( $rootScope.auth._perms >= item.perms  ) {
                    if( ( ! $rootScope.auth._current  && item.perms == 0 ) || $rootScope.auth._current ) {
                        if( typeof redirect == 'undefined' ) {
                            redirect = item.url;
                        }
                        $scope.taba.menu.push(item);
                    }
                }
            })

            if( $rootScope.auth && $rootScope.auth._current ) {
                $rootScope.changeMode('display');
            }

        }
    })

    $rootScope.$on('auth.change', function(){
        if( AuthenticationService.isAuthenticated ) {
            UserService.me(function (data){
                $rootScope.auth = data;
                $rootScope.auth.logged = true;
                $rootScope.auth.is_admin = function() {
                  if( typeof this.Vendedor !== 'undefined' )
                    return true;

                  return false;
                }
                $rootScope.$broadcast('app.ready');

            }, function (err) {
                $rootScope.$broadcast('app.ready');
            });
        } else {
            $location.path("/login");
        }
    })


});
